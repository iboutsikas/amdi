﻿using AMDI.Services.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AMDI.Services.Tests
{
    public class EUExcelParserTests
    {
        private int Commodity_ID = 38456;

        [Theory]
        [InlineData("./EU_Imports.xlsx")]
        [InlineData("./EU_Imports_with_string_residues.xlsx")]
        public async Task Residue_is_parsed_in_numbers_or_strings(string filepath)
        {
            using (IExcelParser parser = new EUExcelParser())
            {
                parser.Initialize(filepath);

                var result = await parser.ParseFileAsync();

                Assert.Equal(Commodity_ID, result.Commodity_ID);
                Assert.Equal(4, result.MRLs.Count);

                foreach (var m in result.MRLs)
                {
                    Assert.Equal("Ingredient" + m.EU_ID, m.Name);
                }
            }
        }

        [Fact]
        public void Reset_throws_if_not_initialized()
        {
            IExcelParser parser = new EUExcelParser();

            Assert.Throws<InvalidOperationException>(() => parser.Reset());
        }

        [Fact]
        public async Task Parse_throws_when_residues_are_random_text()
        {
            using (IExcelParser parser = new EUExcelParser())
            {
                parser.Initialize("./EU_Imports_with_text_in_residues.xlsx");

                await Assert.ThrowsAsync<FormatException>(async () => await parser.ParseFileAsync());
            }
        }

        [Fact]
        public async Task Parse_throws_if_not_initialized()
        {
            IExcelParser parser = new EUExcelParser();

            await Assert.ThrowsAsync<InvalidOperationException>(async () => await parser.ParseFileAsync());
        }
    }
}
