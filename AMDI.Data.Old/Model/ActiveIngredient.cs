﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Data.Model
{
    public class ActiveIngredient
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int EU_ID { get; set; }

        public virtual Category Category { get; set; }
    }
}
