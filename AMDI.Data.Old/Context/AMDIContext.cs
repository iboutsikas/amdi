﻿using AMDI.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Data.Context
{
    public class AMDIContext: DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<ActiveIngredient> ActiveIngredients { get; set; }
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<MRL> MRLs { get; set; }
        public DbSet<MRLProvider> Providers { get; set; }

        public AMDIContext()
            :base("TestContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ActiveIngredient>()
                .HasRequired(a => a.Category)
                .WithMany();

            modelBuilder.Entity<MRL>()
                .HasRequired(m => m.ActiveIngredient)
                .WithMany()
                .HasForeignKey(m => m.ActiveIngredient_ID);

            modelBuilder.Entity<MRL>()
                .HasRequired(m => m.Commodity)
                .WithMany()
                .HasForeignKey(m => m.Commodity_ID);

            modelBuilder.Entity<MRL>()
                .HasRequired(m => m.Provider)
                .WithMany()
                .HasForeignKey(m => m.Provider_ID);
        }
    }
}
