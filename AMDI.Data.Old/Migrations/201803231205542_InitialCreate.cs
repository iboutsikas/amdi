namespace AMDI.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActiveIngredients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        EU_ID = c.Int(nullable: false),
                        Category_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Categories", t => t.Category_ID, cascadeDelete: true)
                .Index(t => t.Category_ID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Shorthand = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Commodities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        EU_ID = c.Int(nullable: false),
                        EU_Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MRLs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Residue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RegisteredAt = c.DateTime(nullable: false),
                        Commodity_ID = c.Int(nullable: false),
                        ActiveIngredient_ID = c.Int(nullable: false),
                        Provider_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ActiveIngredients", t => t.ActiveIngredient_ID, cascadeDelete: true)
                .ForeignKey("dbo.Commodities", t => t.Commodity_ID, cascadeDelete: true)
                .ForeignKey("dbo.MRLProviders", t => t.Provider_ID, cascadeDelete: true)
                .Index(t => t.Commodity_ID)
                .Index(t => t.ActiveIngredient_ID)
                .Index(t => t.Provider_ID);
            
            CreateTable(
                "dbo.MRLProviders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MRLs", "Provider_ID", "dbo.MRLProviders");
            DropForeignKey("dbo.MRLs", "Commodity_ID", "dbo.Commodities");
            DropForeignKey("dbo.MRLs", "ActiveIngredient_ID", "dbo.ActiveIngredients");
            DropForeignKey("dbo.ActiveIngredients", "Category_ID", "dbo.Categories");
            DropIndex("dbo.MRLs", new[] { "Provider_ID" });
            DropIndex("dbo.MRLs", new[] { "ActiveIngredient_ID" });
            DropIndex("dbo.MRLs", new[] { "Commodity_ID" });
            DropIndex("dbo.ActiveIngredients", new[] { "Category_ID" });
            DropTable("dbo.MRLProviders");
            DropTable("dbo.MRLs");
            DropTable("dbo.Commodities");
            DropTable("dbo.Categories");
            DropTable("dbo.ActiveIngredients");
        }
    }
}
