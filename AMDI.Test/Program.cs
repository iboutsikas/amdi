﻿using AMDI.Data.Context;
using AMDI.Data.DependancyInjection;
using AMDI.Domain;
using AMDI.Domain.DependancyInjection;
using AMDI.Domain.Services;
using DryIoc;
using System;
using System.Threading.Tasks;

namespace AMDI.Test
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Container c = new Container(rules => rules.WithTrackingDisposableTransients());
            c.UseInstance(c);
            c.Register<AMDIContextFactory>(Reuse.Singleton);

            c.Register<AMDIContext>(made: Made.Of(r => ServiceInfo.Of<AMDIContextFactory>(), f => f.Create()));
            c.AddDomain();
            DomainMapper.Initialize();

            var providerService = c.Resolve<IMrlProviderService>();

            var items = await providerService.GetAll();

            foreach (var p in items)
            {
                Console.WriteLine(p.Name);
            }

            Console.ReadLine();

        }
    }
}
