﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Models
{
    public class MRL
    {
        public decimal AllowedResidue { get; set; }
        public decimal OldAllowedResidue { get; set; }
        public int IngredientID { get; set; }
        public int CommodityID { get; set; }
        public string IngredientName { get; set; }

        public string Display => $"{IngredientName} - {AllowedResidue}";
    }
}
