﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Models
{
    public class MrlProvider
    {
        public static MrlProvider NYI_Provider = new MrlProvider { ID = 0, Name = "Not Yet Implemented", Logo = "not_implemented.jpg" };
        public int ID { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
    }
}
