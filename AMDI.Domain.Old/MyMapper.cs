﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain
{
    public static class DomainMapper
    {
        private static bool initialized = false;

        public static void Initialize()
        {
            if (initialized) return;

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Data.Model.ActiveIngredient, Models.ActiveIngredient>()
                    .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category.Name));
                cfg.CreateMap<Data.Model.Commodity, Models.Commodity>();
            });
        }
    }
}
