﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Exceptions
{
    public class UnmatchedCommodityException: Exception
    {
        public UnmatchedCommodityException()
            :this("The commodity ID from the parsed file, did not match any commodity in the database")
        {
        }

        public UnmatchedCommodityException(string message) : base(message)
        {
        }

        public UnmatchedCommodityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnmatchedCommodityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
