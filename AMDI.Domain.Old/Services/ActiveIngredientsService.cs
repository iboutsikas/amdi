﻿using AMDI.Data.Context;
using AMDI.Domain.Models;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Services
{
    public class ActiveIngredientsService: IActiveIngredientsService
    {
        public ActiveIngredientsService()
        {
        }

        public Task<List<ActiveIngredient>> GetAll()
        {
            using (var db = new AMDIContext())
            {
                return db.ActiveIngredients
                    .ProjectTo<ActiveIngredient>()
                    .ToListAsync();
            }
        }
    }
}
