﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AMDI.Domain.Models;

namespace AMDI.Domain.Services
{
    public interface ICommoditiesService
    {
        Task<HashSet<Commodity>> GetAll();
        Task<List<Commodity>> GetWhere(Expression<Func<Commodity, bool>> filter);
    }
}