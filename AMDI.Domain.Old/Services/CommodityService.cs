﻿using AMDI.Data.Context;
using AMDI.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using System.Data.Entity;
using System.Linq.Expressions;

namespace AMDI.Domain.Services
{
    public class CommoditiesService: ICommoditiesService
    {
        public CommoditiesService()
        {
        }

        public async Task<HashSet<Commodity>> GetAll()
        {
            using (var db = new AMDIContext())
            {
                var items = await db.Commodities
                    .ProjectTo<Commodity>()
                    .ToListAsync();
                return new HashSet<Commodity>(items);
            }
        }

        public async Task<List<Commodity>> GetWhere(Expression<Func<Commodity, bool>> filter)
        {
            using (var db = new AMDIContext())
            {
                var result = await db.Commodities
                    .ProjectTo<Commodity>()
                    .Where(filter)
                    .ToListAsync();

                return result;
            }
        }

    }
}
