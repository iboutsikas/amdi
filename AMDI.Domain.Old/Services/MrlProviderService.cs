﻿using AMDI.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Subjects;
namespace AMDI.Domain.Services
{
    public class MrlProviderService : IMrlProviderService
    {
        private List<MrlProvider> providers;

        public MrlProviderService()
        {
            providers = new List<MrlProvider>()
            {
                new MrlProvider { ID = 1, Name = "European Commission", Logo = "european_commission.jpg" },
                MrlProvider.NYI_Provider
            };
        }

        public Task<List<MrlProvider>> GetAll()
        {
            // TODO: Make this load from database. There is no logo path and names are messed up in db
            return Task.FromResult(providers);
        }
    }
}
