﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMDI.Domain.Models;

namespace AMDI.Domain.Services
{
    public interface IMrlProviderService
    {
        Task<List<MrlProvider>> GetAll();
    }
}