﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMDI.Data.Context;
using AMDI.Domain.Exceptions;
using AMDI.Domain.Models;
using AMDI.Services.Excel;

namespace AMDI.Domain.Services
{
    public class MrlImportProcessor: IMrlImportProcessor
    {
        public async Task<ProcessedImportResult> ProcessImport(EUParsedFile parsedFile, int providerID)
        {
            return await Task.Run(() =>
            {
                using (var db = new AMDIContext())
                {
                    var commodity = db.Commodities.SingleOrDefault(c => c.EU_ID == parsedFile.Commodity_ID);

                    if (commodity == null)
                    {
                        throw new UnmatchedCommodityException();
                    }

                    var result = new ProcessedImportResult();

                    List<Data.Model.ActiveIngredient> allIngredients = db.ActiveIngredients.ToList();

                    foreach (var mrl in parsedFile.MRLs)
                    {
                        var ingredients = allIngredients
                            .Where(ai => ai.EU_ID == mrl.EU_ID);

                        if (ingredients.Count() == 0)
                            result.UnmatchedRecords.Add(new MRL { AllowedResidue = mrl.AllowedResidue, IngredientName = mrl.Name });
                        else
                        {
                            foreach (var ing in ingredients)
                            {
                                var existingMrl = db.MRLs.Where(m => m.ActiveIngredient_ID == ing.ID
                                                                        && m.Commodity_ID == commodity.ID
                                                                        && m.Provider_ID == providerID
                                                                    ).FirstOrDefault();
                                if (existingMrl == null)
                                {
                                    result.NewRecords.Add(new MRL { AllowedResidue = mrl.AllowedResidue, IngredientName = ing.Name, IngredientID = ing.ID });
                                }
                                else if (mrl.AllowedResidue != existingMrl.Residue)
                                {
                                    result.UpdatedRecords.Add(new MRL { AllowedResidue = mrl.AllowedResidue, IngredientName = ing.Name, IngredientID = ing.ID, OldAllowedResidue = existingMrl.Residue });
                                }
                                else
                                {
                                    result.UnchangedRecords.Add(new MRL { AllowedResidue = mrl.AllowedResidue, IngredientID = ing.ID, IngredientName = ing.Name });
                                }

                            }
                        }
                    }

                    return result;
                }
            });            
        }
    }
}
