﻿using AMDI.Domain.Services;
using DryIoc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.DependancyInjection
{
    public static class Configure
    {
        public static void AddDomain(this Container container)
        {
            container.Register<IMrlProviderService, MrlProviderService>(Reuse.Singleton);
            container.Register<IActiveIngredientsService, ActiveIngredientsService>(Reuse.Singleton);
            container.Register<ICommoditiesService, CommoditiesService>(Reuse.Singleton);
            container.Register<IMrlImportProcessor, MrlImportProcessor>();
        }
    }
}
