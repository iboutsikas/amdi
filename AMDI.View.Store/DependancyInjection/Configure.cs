﻿using AMDI.View.Store;
using DryIoc;

namespace AMDI.Store.DependancyInjection
{
    public static class Configure
    {
        public static void AddStore(this Container container)
        {
            container.Register<IStore, StateStore>(Reuse.Singleton);
        }
    }
}
