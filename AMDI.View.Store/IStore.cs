﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AMDI.Domain.Models;

namespace AMDI.View.Store
{
    public interface IStore
    {
        IObservable<HashSet<Commodity>> Commodities { get; }
        IObservable<List<ActiveIngredient>> Ingredients { get; }
        IObservable<IList<MrlProvider>> Providers { get; }

        void LoadCommodities();
        void LoadCommoditiesWhere(Expression<Func<Commodity, bool>> filter);
        void LoadIngredients();
        void LoadProviders();
    }
}