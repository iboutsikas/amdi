﻿using AMDI.Domain.Models;
using AMDI.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View.Store
{
    public class StateStore: IStore
    {
        private readonly IActiveIngredientsService ingredientsService;
        private readonly ICommoditiesService commoditiesService;
        private readonly IMrlProviderService providerService;
        private readonly BehaviorSubject<List<ActiveIngredient>> ingredientsSubject;
        private readonly BehaviorSubject<HashSet<Commodity>> commoditiesSubject;
        private readonly BehaviorSubject<List<MrlProvider>> providersSubject;

        private HashSet<Commodity> commodities;
        
        

        private object commoditiesLock;

        public IObservable<List<ActiveIngredient>> Ingredients => ingredientsSubject;
        public IObservable<HashSet<Commodity>> Commodities => commoditiesSubject;
        public IObservable<IList<MrlProvider>> Providers => providersSubject;


        public StateStore(IActiveIngredientsService ingredientsService,
            ICommoditiesService commoditiesService,
            IMrlProviderService providerService)
        {
            this.ingredientsService = ingredientsService;
            this.commoditiesService = commoditiesService;
            this.providerService = providerService;

            commoditiesLock = new object();
            commodities = new HashSet<Commodity>();
            

            this.ingredientsSubject = new BehaviorSubject<List<ActiveIngredient>>(new List<ActiveIngredient>());
            this.providersSubject = new BehaviorSubject<List<MrlProvider>>(new List<MrlProvider>());
            this.commoditiesSubject = new BehaviorSubject<HashSet<Commodity>>(commodities);

            
        }

        public void LoadIngredients()
        {
            Task.Run(async () =>
            {
                var list = await ingredientsService.GetAll()
                    .ConfigureAwait(false);
                ingredientsSubject.OnNext(list);
            });
        }

        public void LoadCommodities()
        {
            Task.Run(async () =>
            {
                var list = await commoditiesService.GetAll()
                    .ConfigureAwait(false);

                commoditiesSubject.OnNext(list);
            });
        }

        public void LoadCommoditiesWhere(Expression<Func<Commodity, bool>> filter)
        {
            Task.Run(async () =>
            {
                var items = await commoditiesService.GetWhere(filter);

                lock (commoditiesLock)
                {
                    foreach (var item in items)
                    {
                        commodities.Remove(item);
                        commodities.Add(item);
                    }
                }

                commoditiesSubject.OnNext(commodities);
            });
        }

        public void LoadProviders()
        {
            Task.Run(async () => {
                var providers = await providerService.GetAll()
                .ConfigureAwait(false);

                providersSubject.OnNext(providers);
            });
        }
    }
}
