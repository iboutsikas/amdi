﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AMDI.API.Features.Commodity;
using AMDI.API.Features.Commodity.Commands;
using AMDI.API.Features.Commodity.Queries;
using AMDI.API.Features.Commodity.ViewModels;
using AMDI.Data.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AMDI.API.Controllers
{
    [Produces("application/json")]
    [Route("api/commodities")]
    public class CommoditiesController: Controller
    {
        private readonly AMDIContext context;

        public CommoditiesController(AMDIContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]int page, [FromQuery]int pageSize)
        {
            var query = new ListCommoditiesQuery(context);

            if (page > 0)
                page = page - 1;

            var items = await query.Execute(page, pageSize);
            return Ok(items);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var query = new ListSingleCommodity(context);

            var item = await query.Execute(id);

            if (item == null)
            {
                var error = new ErrorDocument
                {
                    StatusCode = (int)HttpStatusCode.NotFound,
                    Message = $"Could not find Commodity with id of {id}"
                };
                return NotFound(error);
            }

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PostViewModel viewModel)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var command = new CreateCommodityCommand(context);

                    var entity = await command.Execute(viewModel.Name, viewModel.EU_ID);

                    transaction.Commit();
                    return Ok(entity);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]PutViewModel vm)
        {
            if (id != vm.ID)
            {
                return BadRequest("Object id and URL id do not match");
            }

            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var command = new UpdateCommodityCommand(context, vm);

                    var entity = await command.Execute();

                    if (entity == null)
                    {
                        var error = new ErrorDocument
                        {
                            StatusCode = (int)HttpStatusCode.NotFound,
                            Message = $"Could not find Active Ingredient with id of {id}"
                        };

                        transaction.Rollback();
                        return NotFound(error);
                    }

                    transaction.Commit();
                    return Ok(entity);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var command = new DeleteCommodityCommand(context);

                    await command.Execute(id);

                    transaction.Commit();
                    return NoContent();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }
        }
    }
}