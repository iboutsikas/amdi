﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AMDI.API.Features;
using AMDI.API.Features.ActiveIngredient;
using AMDI.API.Features.ActiveIngredient.Commands;
using AMDI.API.Features.ActiveIngredient.ViewModels;
using AMDI.Data.Context;
using AMDI.Domain.Models;
using AMDI.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AMDI.API.Controllers
{
    [Produces("application/json")]
    [Route("api/active-ingredients")]
    public class ActiveIngredientsController : Controller
    {
        private readonly AMDIContext context;

        public ActiveIngredientsController(AMDIContext context)
        {
            this.context = context;
        }
        // GET: api/ActiveIngredient
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]int page, [FromQuery]int pageSize)
        {

            if (page > 0)
                page = page - 1;

            IQuery<List<ActiveIngredient>> query = new ListIngredientsQuery(context, page, pageSize);
            var items = await query.Execute();
            return Ok(items);
        }

        // GET: api/ActiveIngredient/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            IQuery<ActiveIngredient> query = new ListSingleIngredient(context, id);

            var item = await query.Execute();

            if (item == null)
            {
                var error = new ErrorDocument {
                    StatusCode = (int)HttpStatusCode.NotFound,
                    Message =  $"Could not find Active Ingredient with id of {id}"
                };
                return NotFound(error);
            }

            return Ok(item);
        }
        
        // POST: api/ActiveIngredient
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PostViewModel viewModel)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var command = new CreateIngredientCommand(context);

                    var entity = await command.Execute(viewModel.Name, viewModel.EU_ID, viewModel.Category_ID);

                    transaction.Commit();
                    return Ok(entity);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
                
            }
        }

        // PUT: api/ActiveIngredient/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]PutViewModel vm)
        {
            if (id != vm.ID)
            {
                return BadRequest("Object id and URL id do not match");
            }

            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var command = new UpdateIngredientCommand(context, vm);

                    var entity = await command.Execute();

                    if (entity == null)
                    {
                        var error = new ErrorDocument
                        {
                            StatusCode = (int)HttpStatusCode.NotFound,
                            Message = $"Could not find Active Ingredient with id of {id}"
                        };

                        transaction.Rollback();
                        return NotFound(error);
                    }

                    transaction.Commit();
                    return Ok(entity);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var command = new DeleteIngredientCommand(context);

                    await command.Execute(id);

                    transaction.Commit();
                    return NoContent();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }
        }
    }
}
