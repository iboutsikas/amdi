﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMDI.API.Features.Mrl.Commands;
using AMDI.API.Features.Mrl.Queries;
using AMDI.API.Features.Mrl.ViewModels;
using AMDI.Data.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AMDI.API.Controllers
{
    [Produces("application/json")]
    [Route("api/mrls")]
    public class MrlController: Controller
    {
        private readonly AMDIContext context;

        public MrlController(AMDIContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] MrlGetViewModel vm)
        {
            DateTime from;
            var hasFrom = DateTime.TryParse(vm.From, out from);

            DateTime to;
            var hasTo = DateTime.TryParse(vm.To, out to);

            if (from > to)
            {
                return BadRequest("From date cannot be after to date");
            }

            List<Domain.Models.MRL> result;

            if (hasFrom || hasTo)
            {
                var query = new SearchMrlWithHistoricalDataQuery(context);
                result = await query.Execute(vm.iids, vm.cids, from, to);
            }
            else
            {
                var query = new SearchMrlQuery(context);
                result = await query.Execute(vm.iids, vm.cids);
            }

            if (result.Count == 0)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] MrlPutViewModel vm)
        {
            if (id != vm.ID)
            {
                return BadRequest("Object id and URL id do not match");
            }

            var command = new UpdateMrlCommand(context);

            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    var entity = await command.Execute(vm.ID, vm.Residue);

                    if (entity == null)
                    {
                        transaction.Rollback();
                        return NotFound();
                    }
                    transaction.Commit();
                    return Ok(entity);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
                
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var command = new DeleteMrlCommand(context, id);

                    await command.Execute();

                    transaction.Commit();
                    return NoContent();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    return BadRequest(e.Message);
                }
            }
        }
    }
}