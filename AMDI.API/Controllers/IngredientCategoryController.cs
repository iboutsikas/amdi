﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMDI.API.Features.IngredientCategory;
using AMDI.Data.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AMDI.API.Controllers
{
    [Produces("application/json")]
    [Route("api/ingredient-categories")]
    public class IngredientCategoryController : Controller
    {
        private readonly AMDIContext context;

        public IngredientCategoryController(AMDIContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]int page, [FromQuery]int pageSize)
        {
            var query = new ListCategoriesQuery(context);

            if (page > 0)
                page = page - 1;

            var items = await query.Execute(page, pageSize);
            return Ok(items);
        }
    }
}