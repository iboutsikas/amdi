﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.API
{
    public class ErrorDocument
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }
    }
}
