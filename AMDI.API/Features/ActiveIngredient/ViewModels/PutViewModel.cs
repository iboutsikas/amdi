﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.API.Features.ActiveIngredient.ViewModels
{
    public class PutViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Category_ID { get; set; }
        public int EU_ID { get; set; }
    }
}
