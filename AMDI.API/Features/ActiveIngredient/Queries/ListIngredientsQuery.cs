﻿using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.ActiveIngredient
{
    public class ListIngredientsQuery: IQuery<List<DomainModel.ActiveIngredient>>
    {
        private readonly AMDIContext context;
        private readonly int page;
        private readonly int pageSize;
        private readonly Expression<Func<DataModel.ActiveIngredient, bool>> filter;

        public ListIngredientsQuery(AMDIContext context,
            int page = 0,
            int pageSize = 0,
            Expression<Func<DataModel.ActiveIngredient, bool>> filter = null)
        {
            this.context = context;
            this.page = page;
            this.pageSize = pageSize;
            this.filter = filter;
        }

        public Task<List<DomainModel.ActiveIngredient>> Execute()
        {
            var query = context.ActiveIngredients
                .Include(a => a.Category)
                .AsNoTracking();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (pageSize != 0)
            {
                query = query.Skip(page * pageSize)
                    .Take(pageSize);
            }

            return query
                    .Select(a => Mapper.Map<DomainModel.ActiveIngredient>(a))
                    .ToListAsync();
        }
    }
}
