﻿using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.ActiveIngredient
{
    public class ListSingleIngredient: IQuery<DomainModel.ActiveIngredient>
    {
        private readonly AMDIContext context;
        private readonly int id;

        public ListSingleIngredient(AMDIContext context, int id)
        {
            this.context = context;
            this.id = id;
        }

        public Task<DomainModel.ActiveIngredient> Execute()
        {
            return context.ActiveIngredients
                .AsNoTracking()
                .Select(a => Mapper.Map<DomainModel.ActiveIngredient>(a))
                .FirstOrDefaultAsync(a => a.ID == id);
        }
    }
}
