﻿using AMDI.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel = AMDI.Domain.Models;
using DataModel = AMDI.Data.Model;
using AutoMapper;

namespace AMDI.API.Features.ActiveIngredient.Commands
{
    public class CreateIngredientCommand
    {
        private readonly AMDIContext context;

        public CreateIngredientCommand(AMDIContext context)
        {
            this.context = context;
        }

        public async Task<DomainModel.ActiveIngredient> Execute(string name, int euId, int categoryId)
        {
            var entity = new DataModel.ActiveIngredient
            {
                Name = name,
                EU_ID = euId,
                Category_ID = categoryId
            };

            context.Add(entity);
           
            await context.SaveChangesAsync();
            context.Entry(entity)
                .Reference(e => e.Category)
                .Load();

            return Mapper.Map<DomainModel.ActiveIngredient>(entity);
        }
    }
}
