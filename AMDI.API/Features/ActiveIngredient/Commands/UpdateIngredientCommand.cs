﻿using AMDI.API.Features.ActiveIngredient.ViewModels;
using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.ActiveIngredient.Commands
{
    public class UpdateIngredientCommand
    {
        private readonly AMDIContext context;
        private readonly PutViewModel vm;

        public UpdateIngredientCommand(AMDIContext context, PutViewModel vm)
        {
            this.context = context;
            this.vm = vm;
        }

        public async Task<DomainModel.ActiveIngredient> Execute()
        {
            var entity = await context.ActiveIngredients
                .FirstOrDefaultAsync(a => a.ID == vm.ID);

            if (entity == null)
            {
                // Bail out early since the entity was not found
                return null;
            }

            if (entity.Name != vm.Name && vm.Name != default(string))
            {
                entity.Name = vm.Name;
            }

            if (entity.EU_ID != vm.EU_ID && vm.EU_ID != default(int))
            {
                entity.EU_ID = vm.EU_ID;
            }

            if (entity.Category_ID != vm.Category_ID && vm.Category_ID != default(int))
            {
                entity.Category_ID = vm.Category_ID;
            }

            entity.ModifiedAt = DateTime.Now;

            context.ActiveIngredients.Update(entity);
            await context.SaveChangesAsync();

            entity = await context.ActiveIngredients
                .Include(a => a.Category)
                .FirstOrDefaultAsync(a => a.ID == vm.ID);

            return Mapper.Map<DomainModel.ActiveIngredient>(entity);
        }
    }
}
