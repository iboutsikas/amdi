﻿using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.IngredientCategory
{
    public class ListCategoriesQuery
    {
        private readonly AMDIContext context;

        public ListCategoriesQuery(AMDIContext context)
        {
            this.context = context;
        }

        public Task<List<DomainModel.Category>> Execute(
            int page = 0,
            int pageSize = 0,
            Expression<Func<DataModel.Category, bool>> filter = null)
        {
            var query = context.Categories
                .AsNoTracking();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (pageSize != 0)
            {
                query = query.Skip(page * pageSize)
                    .Take(pageSize);
            }

            return query
                    .Select(a => Mapper.Map<DomainModel.Category>(a))
                    .ToListAsync();
        }
    }
}
