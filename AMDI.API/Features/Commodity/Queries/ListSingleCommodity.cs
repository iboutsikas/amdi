﻿using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.Commodity
{
    public class ListSingleCommodity
    {
        private readonly AMDIContext context;

        public ListSingleCommodity(AMDIContext context)
        {
            this.context = context;
        }

        public Task<DomainModel.Commodity> Execute(int id)
        {
            return context.Commodities
                .AsNoTracking()
                .Select(a => Mapper.Map<DomainModel.Commodity>(a))
                .FirstOrDefaultAsync(a => a.ID == id);
        }
    }
}
