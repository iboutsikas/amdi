﻿using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.Commodity.Queries
{
    public class ListCommoditiesQuery
    {
        private readonly AMDIContext context;

        public ListCommoditiesQuery(AMDIContext context)
        {
            this.context = context;
        }

        public Task<List<DomainModel.Commodity>> Execute(
           int page = 0,
           int pageSize = 0,
           Expression<Func<DataModel.Commodity, bool>> filter = null)
        {
            var query = context.Commodities
                .AsNoTracking();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (pageSize != 0)
            {
                query = query.Skip(page * pageSize)
                    .Take(pageSize);
            }

            return query
                    .Select(a => Mapper.Map<DomainModel.Commodity>(a))
                    .ToListAsync();
        }
    }
}
