﻿using AMDI.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;

namespace AMDI.API.Features.Commodity.Commands
{
    public class DeleteCommodityCommand
    {
        private readonly AMDIContext context;

        public DeleteCommodityCommand(AMDIContext context)
        {
            this.context = context;
        }

        public async Task Execute(int id)
        {
            var entity = new DataModel.Commodity
            {
                ID = id
            };

            context.Entry(entity).State = EntityState.Deleted;
            await context.SaveChangesAsync();
        }
    }
}
