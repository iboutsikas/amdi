﻿using AMDI.API.Features.Commodity.ViewModels;
using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.Commodity.Commands
{
    public class UpdateCommodityCommand
    {
        private readonly AMDIContext context;
        private readonly PutViewModel vm;

        public UpdateCommodityCommand(AMDIContext context, PutViewModel vm)
        {
            this.context = context;
            this.vm = vm;
        }

        public async Task<DomainModel.Commodity> Execute()
        {
            var entity = await context.Commodities
                .FirstOrDefaultAsync(a => a.ID == vm.ID);

            if (entity == null)
            {
                // Bail out early since the entity was not found
                return null;
            }

            if (entity.Name != vm.Name)
            {
                entity.Name = vm.Name;
            }

            if (entity.EU_ID != vm.EU_ID)
            {
                entity.EU_ID = vm.EU_ID;
            }

            entity.ModifiedAt = DateTime.Now;

            context.Commodities.Update(entity);
            await context.SaveChangesAsync();
            
            return Mapper.Map<DomainModel.Commodity>(entity);
        }
    }
}
