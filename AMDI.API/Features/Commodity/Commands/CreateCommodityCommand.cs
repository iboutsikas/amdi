﻿using AMDI.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModel = AMDI.Domain.Models;
using DataModel = AMDI.Data.Model;
using AutoMapper;

namespace AMDI.API.Features.Commodity.Commands
{
    public class CreateCommodityCommand
    {
        private readonly AMDIContext context;

        public CreateCommodityCommand(AMDIContext context)
        {
            this.context = context;
        }

        public async Task<DomainModel.Commodity> Execute(string name, int euId)
        {
            var entity = new DataModel.Commodity
            {
                Name = name,
                EU_ID = euId
            };

            context.Add(entity);
           
            await context.SaveChangesAsync();

            return Mapper.Map<DomainModel.Commodity>(entity);
        }
    }
}
