﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.API.Features
{
    public interface ICommand<TResult> where TResult: class
    {
        Task<TResult> Execute();
    }

    public interface ICommand
    {
        Task Execute();
    }
}
