﻿using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.Mrl.Commands
{
    public class UpdateMrlCommand
    {
        private readonly AMDIContext context;

        public UpdateMrlCommand(AMDIContext context)
        {
            this.context = context;
        }

        public async Task<DomainModel.MRL> Execute(int id, decimal residue)
        {
            var entity = await context.MRLs.FirstOrDefaultAsync(e => e.ID == id);

            if (entity == null)
            {
                return null;
            }

            entity.Residue = residue;

            context.MRLs.Update(entity);
            await context.SaveChangesAsync();

            entity = await context.MRLs
                .Include(m => m.ActiveIngredient)
                .Include(m => m.Commodity)
                .FirstOrDefaultAsync(e => e.ID == id);

            return Mapper.Map<DomainModel.MRL>(entity);
        }
    }
}
