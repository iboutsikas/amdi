﻿using AMDI.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.API.Features.Mrl.Commands
{
    public class DeleteMrlCommand
    {
        private readonly AMDIContext context;
        private readonly int id;

        public DeleteMrlCommand(AMDIContext context, int id)
        {
            this.context = context;
            this.id = id;
        }

        public async Task Execute()
        {
            var entity = new Data.Model.MRL()
            {
                ID = id
            };

            context.Entry(entity).State = EntityState.Deleted;
            await context.SaveChangesAsync();
        }
    }
}
