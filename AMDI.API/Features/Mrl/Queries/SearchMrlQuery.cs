﻿using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;
using DomainModel = AMDI.Domain.Models;

namespace AMDI.API.Features.Mrl.Queries
{
    public class SearchMrlQuery
    {
        private readonly AMDIContext context;

        public SearchMrlQuery(AMDIContext context)
        {
            this.context = context;
        }

        public async Task<List<DomainModel.MRL>> Execute(int[] ingredient_IDs, int[] commodity_IDs)
        {
            // TODO: -5 is a bullshit hardcoded value. Refactor this to something else soon
            bool includeAllIngredients = ingredient_IDs.Contains(-5) || ingredient_IDs.Length == 0;
            bool includeAllCommodities = commodity_IDs.Contains(-5) || commodity_IDs.Length == 0;

            var query = this.context.MRLs
                .AsNoTracking();

            if (!includeAllIngredients && !includeAllCommodities)
            {
                query = query.Where(m => ingredient_IDs.Contains(m.ActiveIngredient_ID)
                         && commodity_IDs.Contains(m.Commodity_ID));
            }
            else if (includeAllIngredients ^ includeAllCommodities)
            {
                if (includeAllIngredients)
                {
                    query = query.Where(m => commodity_IDs.Contains(m.Commodity_ID));
                }

                if (includeAllCommodities)
                {
                    query = query.Where(m => ingredient_IDs.Contains(m.ActiveIngredient_ID));
                }
            }

            var result = await query
                .Include(m => m.Commodity)
                .Include(m => m.ActiveIngredient)                
                .Select(m => Mapper.Map<DomainModel.MRL>(m))
                .ToListAsync();

            return result;
        }
    }
}
