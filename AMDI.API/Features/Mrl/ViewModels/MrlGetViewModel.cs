﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.API.Features.Mrl.ViewModels
{
    public class MrlGetViewModel
    {
        public int[] cids { get; set; } = new int[0];
        public int[] iids { get; set; } = new int[0];
        public string From { get; set; }
        public string To { get; set; }
    }
}
