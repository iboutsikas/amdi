﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.API.Features.Mrl.ViewModels
{
    public class MrlPutViewModel
    {
        public int ID { get; set; }
        public decimal Residue { get; set; }
    }
}
