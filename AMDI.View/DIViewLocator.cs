﻿using DryIoc;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View
{
    public class DIViewLocator : IViewLocator, IEnableLogger
    {
        private readonly Container container;

        public DIViewLocator(Container container)
        {
            this.container = container;
        }
        public IViewFor ResolveView<T>(T viewModel, string contract = null) where T : class
        {
            var viewModelName = viewModel.GetType().FullName;
            var viewTypeName = viewModelName.TrimEnd("Model".ToCharArray());

            try
            {
                var view = container.Resolve<IViewFor<T>>();

                var viewType = view.GetType();

                if (viewType == null)
                {
                    this.Log().Error($"Could not find the view {viewTypeName} for view model {viewModelName}.");
                    return null;
                }
                return Activator.CreateInstance(viewType) as IViewFor;
            }
            catch (Exception)
            {
                this.Log().Error($"Could not instantiate view {viewTypeName}.");
                throw;
            }
        }
    }
}
