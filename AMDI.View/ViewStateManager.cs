﻿using AMDI.Domain.Models;
using AMDI.Services.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View
{
    public class ViewStateManager: IViewStateManager
    {
        private BehaviorSubject<EUParsedFile> euMrlSubject;
        private BehaviorSubject<MrlProvider> providerSubject;
        

        public IObservable<EUParsedFile> EuImport => euMrlSubject;
        public IObservable<MrlProvider> MrlProvider => providerSubject;

        public ViewStateManager()
        {
            euMrlSubject = new BehaviorSubject<EUParsedFile>(null);
            providerSubject = new BehaviorSubject<MrlProvider>(null);
        }

        public void AddNewEuImport(EUParsedFile parsedFile)
        {
            this.euMrlSubject.OnNext(parsedFile);
        }

        public void SelectProvider(MrlProvider provider)
        {
            providerSubject.OnNext(provider);
        }
    }
}
