﻿using System;
using System.Collections.Generic;
using AMDI.Domain.Models;
using AMDI.Services.Excel;

namespace AMDI.View
{
    public interface IViewStateManager
    {
        IObservable<EUParsedFile> EuImport { get; }
        IObservable<MrlProvider> MrlProvider { get; }
        void AddNewEuImport(EUParsedFile parsedFile);
        void SelectProvider(MrlProvider provider);
    }
}