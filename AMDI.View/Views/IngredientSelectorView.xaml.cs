﻿using AMDI.View.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AMDI.View.Views
{
    /// <summary>
    /// Interaction logic for IngredientSelectorView.xaml
    /// </summary>
    public partial class IngredientSelectorView: UserControl, IViewFor<IngredientSelectorViewModel>
    {
        public IngredientSelectorView()
        {
            InitializeComponent();
            this.WhenAnyValue(x => x.ViewModel).BindTo(this, x => x.DataContext);

            this.WhenActivated(d =>
            {
                d(this.Bind(ViewModel, vm => vm.SearchFilter, @this => @this.SearchBox.Text));
            });
        }

        public IngredientSelectorViewModel ViewModel { get; set; }
        object IViewFor.ViewModel { get => ViewModel; set => ViewModel = (IngredientSelectorViewModel)value; }
    }
}
