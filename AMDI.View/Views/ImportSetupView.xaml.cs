﻿using AMDI.View.ViewModels;
using MahApps.Metro.Controls.Dialogs;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AMDI.View.Views
{
    /// <summary>
    /// Interaction logic for FileSelectionView.xaml
    /// </summary>
    public partial class ImportSetupView : UserControl, IViewFor<ImportSetupViewModel>
    {
        public ImportSetupView(ImportSetupViewModel viewModel)
        {
            InitializeComponent();
            this.WhenAnyValue(x => x.ViewModel).BindTo(this, x => x.DataContext);
            ViewModel = viewModel;

            this.WhenActivated(d =>
            {
                d(this.OneWayBind(ViewModel, vm => vm.providerSelectorVM, @this => @this.ProviderSelector.DataContext));

                d(this.OneWayBind(ViewModel, vm => vm.IsProviderSelected, @this => @this.FileSelector.Visibility,
                    value => value ? Visibility.Visible : Visibility.Collapsed));
                d(this.BindCommand(ViewModel, vm => vm.SelectFileCommand, @this => @this.BrowseBtn));
                d(this.Bind(ViewModel, vm => vm.SelectedFilePath, @this => @this.PathTextBox.Text));
                d(this.BindCommand(ViewModel, vm => vm.ImportFileCommand, @this => @this.ImportBtn));
            });
        }

        public ImportSetupViewModel ViewModel { get; set; }
        object IViewFor.ViewModel { get => ViewModel; set => ViewModel = (ImportSetupViewModel)value; }
    }
}
