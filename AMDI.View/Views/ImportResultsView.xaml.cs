﻿using AMDI.View.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AMDI.View.Views
{
    /// <summary>
    /// Interaction logic for ImportResultsView.xaml
    /// </summary>
    public partial class ImportResultsView : UserControl, IViewFor<ImportResultsViewModel>
    {
        private bool isFirstLoad = true;

        public ImportResultsView(ImportResultsViewModel viewModel)
        {
            InitializeComponent();
            this.WhenAnyValue(x => x.ViewModel).BindTo(this, x => x.DataContext);
            ViewModel = viewModel;
            this.WhenActivated(d =>
            {
                d(this.OneWayBind(ViewModel, vm => vm.Loading, @this => @this.LoadingRing.Visibility,
                    value => value ? Visibility.Visible : Visibility.Collapsed));

                d(this.OneWayBind(ViewModel, vm => vm.Loading, @this => @this.ControlContent.Visibility,
                    value => value ? Visibility.Collapsed: Visibility.Visible));

                d(this.OneWayBind(ViewModel, vm => vm.Commodity, @this => @this.CommodityName.Text, commodity => {
                    return (commodity == null) ? "" : commodity.Name;
                }));

                d(this.OneWayBind(ViewModel, vm => vm.NewRecords, @this => @this.NewRecords.ItemsSource));
                d(this.OneWayBind(ViewModel, vm => vm.UpdatedRecords, @this => @this.UpdatedRecords.ItemsSource));
                d(this.OneWayBind(ViewModel, vm => vm.UnchangedRecords, @this => @this.UnchangedRecords.ItemsSource));
                d(this.OneWayBind(ViewModel, vm => vm.UnmatchedRecords, @this => @this.UnmatchedRecords.ItemsSource));
                d(this.BindCommand(ViewModel, vm => vm.ProcessResultsCommand, @this => @this.ContinueButton));
            });
        }

        public ImportResultsViewModel ViewModel { get; set; }
        object IViewFor.ViewModel { get => ViewModel; set => ViewModel = (ImportResultsViewModel)value; }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (isFirstLoad)
            {
                ViewModel.StartObservingData();
                isFirstLoad = false;
            }
        }
    }
}
