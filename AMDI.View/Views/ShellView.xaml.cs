﻿using AMDI.View.ViewModels;
using MahApps.Metro.Controls;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AMDI.View.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : IViewFor<ShellViewModel>
    {
        public ShellView(ShellViewModel viewModel)
        {
            InitializeComponent();

            this.WhenAnyValue(x => x.ViewModel).BindTo(this, v => v.DataContext);
            ViewModel = viewModel;

            this.WhenActivated(d =>
            {
                d(this.Bind(this.ViewModel, vm => vm.Router, @this => @this.ContentHost.Router));
                d(this.BindCommand(ViewModel, vm => vm.NewImportCommand, @this => @this.NewImportBtn));
               
                this.ViewModel.NavigateToFirstView();
                
            });
        }

        public ShellViewModel ViewModel { get; set; }
        object IViewFor.ViewModel {
            get => ViewModel;
            set => ViewModel = (ShellViewModel)value;
        }
    }
}
