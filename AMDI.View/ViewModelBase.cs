﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View
{
    public class ViewModelBase: ReactiveObject
    {
        protected CompositeDisposable subscriptions;
        public ViewModelBase()
        {
            subscriptions = new CompositeDisposable();
        }

        ~ViewModelBase()
        {
            subscriptions.Dispose();
        }
    }
}
