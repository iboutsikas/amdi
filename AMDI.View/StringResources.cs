﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View
{
    internal static class StringResources
    {
        internal static readonly string ImportFormatError = "el_ImportFormatError";
        internal static readonly string NewImportConfirmationMessage = "el_NewImportConfirmation";
    }
}
