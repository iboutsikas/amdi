﻿using AMDI.Domain;
using AMDI.Domain.DependancyInjection;
using AMDI.Store.DependancyInjection;
using AMDI.View.ViewModels;
using AMDI.View.Views;
using DryIoc;
using MahApps.Metro.Controls.Dialogs;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using AMDI.Data.DependancyInjection;

namespace AMDI.View
{
    internal class Bootstrapper
    {
        public static Container Container { get; private set; }
        private ShellView shellView;
        public Bootstrapper()
        {
            Container = new Container(rules => rules.WithTrackingDisposableTransients());
            Container.UseInstance<Container>(Container);
        }

        public void Initialize()
        {
            Container.Register<ILogger, DebugLogger>(Reuse.Singleton);
            // Register our custom ViewLocator First
            Container.Register<IViewLocator, DIViewLocator>(Reuse.Singleton);
            Container.Register<IViewStateManager, ViewStateManager>(Reuse.Singleton);
            Container.UseInstance<IDialogCoordinator>(DialogCoordinator.Instance);
            RegisterViewModels();
            RegisterViews();

            Container.AddDataLayer();
            Container.AddDomain();
            DomainMapper.Initialize();
            Container.AddStore();

            // Replace Splat's current resolver, to delegate resolution to DryIoc
            Locator.Current = new FuncDependencyResolver(
                (service, contract) =>
                {
                    if (string.IsNullOrWhiteSpace(contract))
                        return Container.ResolveMany(service, serviceKey: contract);
                    return Container.ResolveMany(service);
                },
                (factory, service, contract) =>
                {
                    if (!string.IsNullOrWhiteSpace(contract))
                        Container.RegisterDelegate(service, r => factory(), serviceKey: contract);
                    Container.RegisterDelegate(service, r => factory());
                }
            );
        }

        private void RegisterViewModels()
        {
            var vms = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(type => type.BaseType == typeof(ViewModelBase))
                .ToList();

            foreach(var vm in vms)
            {
                if (vm.GetImplementedInterfaces().Contains(typeof(IScreen)))
                {
                    Container.Register(vm, Reuse.Singleton);
                    Container.RegisterMapping(typeof(IScreen), vm);
                }
                else
                {
                    Container.Register(vm, Reuse.InResolutionScope);
                }
            }
        }

        private void RegisterViews()
        {
            var viewTypes = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(type =>
                    type.IsClass && 
                    !type.IsAbstract &&
                    type.GetImplementedInterfaces().Any(i =>
                        i.IsGeneric() &&
                        i.GetGenericTypeDefinition() == typeof(IViewFor<>)
                    )
                )
                .ToList();

            foreach (var type in viewTypes)
            {
                var iviewtype = type.GetImplementedInterfaces().First(i =>
                        i.IsGeneric() &&
                        i.GetGenericTypeDefinition() == typeof(IViewFor<>)
                    );
                Container.Register(iviewtype, type);
            }
        }

        public void OnStartup(StartupEventArgs e)
        {
            shellView = (ShellView)Container.Resolve<IViewFor<ShellViewModel>>();
            shellView.Show();
        }

        public void OnExit(ExitEventArgs e)
        {
            Container.Dispose();
        }
    }
}
