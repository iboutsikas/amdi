﻿using DryIoc;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View
{
    public static class RouterExtensions
    {
        public static IObservable<IRoutableViewModel> Navigate<TViewModel>(this RoutingState router) where TViewModel: IRoutableViewModel
        {
            var vm = Bootstrapper.Container.Resolve<TViewModel>();
            return router.Navigate.Execute(vm);
        }
    }
}
