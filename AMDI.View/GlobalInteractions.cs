﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;

namespace AMDI.View
{
    public static class GlobalInteractions
    {
        public static readonly Interaction<string, Unit> DisplayedToUserErrors = new Interaction<string, Unit>();
    }
}
