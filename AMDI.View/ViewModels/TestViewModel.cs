﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;
using Splat;

namespace AMDI.View.ViewModels
{
    public class TestViewModel : ViewModelBase, IRoutableViewModel, IEnableLogger
    {
        public string UrlPathSegment => "Test";

        public IScreen HostScreen { get; private set; }

        public TestViewModel(IScreen host)
        {
            HostScreen = host;
            this.Log().Debug("Test View Model online");
        }

    }
}
