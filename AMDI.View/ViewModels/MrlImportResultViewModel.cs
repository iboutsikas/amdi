﻿using AMDI.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View.ViewModels
{
    public class MrlImportResultViewModel:  ViewModelBase
    {
        private readonly MRL mrl;

        public MrlImportResultViewModel(MRL mrl)
        {
            this.mrl = mrl;
        }
    }
}
