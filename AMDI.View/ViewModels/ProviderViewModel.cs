﻿using AMDI.Domain.Models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View.ViewModels
{
    
    public class ProviderViewModel: ViewModelBase
    {
        private readonly MrlProvider provider;
        private readonly ReactiveCommand<Unit, Unit> clickedCommand;
        private readonly Subject<MrlProvider> selectedProviderSubject;

        public ReactiveCommand<Unit, Unit> ClickedCommand => clickedCommand;
        public IObservable<MrlProvider> OnSelected => selectedProviderSubject;
        public string Logopath => String.Format("/AMDI.View;component/Resources/{0}", provider.Logo);
        public string DisplayName => provider.Name;

        public ProviderViewModel(MrlProvider provider)
        {
            this.provider = provider;
            this.selectedProviderSubject = new Subject<MrlProvider>();
            this.clickedCommand = ReactiveCommand.Create(() => this.selectedProviderSubject.OnNext(this.provider));
        }

        
    }
}
