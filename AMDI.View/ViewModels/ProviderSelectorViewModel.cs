﻿using AMDI.Domain.Models;
using AMDI.Domain.Services;
using AMDI.View.Store;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View.ViewModels
{
    public class ProviderSelectorViewModel: ViewModelBase
    {
        private readonly IStore store;
        private readonly IViewStateManager stateManager;
        private readonly BehaviorSubject<bool> readySubject;
        private readonly ObservableAsPropertyHelper<IList<ProviderViewModel>> _providers;

        private CompositeDisposable vmSubs;


        public IObservable<bool> OnReady => readySubject;
        public IList<ProviderViewModel> Providers => _providers.Value;

        public ProviderSelectorViewModel(IStore store, IViewStateManager stateManager)
        {
            this.store = store;
            this.stateManager = stateManager;
            this.readySubject = new BehaviorSubject<bool>(false);
            this.vmSubs = new CompositeDisposable();

            this.store.LoadProviders();

            this.WhenAnyObservable(x => x.store.Providers)
                .Do(_ =>
                {
                    vmSubs.Dispose();
                    vmSubs = new CompositeDisposable();
                })
                .SelectMany(providers => providers.ToObservable()
                    .Select(p => {
                        var vm = new ProviderViewModel(p);
                        vmSubs.Add(vm.OnSelected.Subscribe(provider => stateManager.SelectProvider(provider)));
                        return vm;
                    })
                    .ToList()
                )
                .ToProperty(this, x => x.Providers, out _providers);
        }
    }
}
