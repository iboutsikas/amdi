﻿using AMDI.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View.ViewModels.Design
{
    public class DesignFileSelectionViewModel
    {
        public List<MrlProvider> providers { get; set; }
        public List<ProviderViewModel> Providers { get; set; }

        public DesignFileSelectionViewModel()
        {
            providers = new List<MrlProvider>()
            {
                new MrlProvider { Name = "European Commission", Logo = "european_commission.jpg" },
                new MrlProvider { Name = "Not Yet Implemented", Logo = "not_implemented.jpg" }
            };

            Providers = new List<ProviderViewModel>();
            
            foreach (var p in providers)
            {
                Providers.Add(new ProviderViewModel(p));
            }
        }
    }
}
