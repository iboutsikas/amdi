﻿using AMDI.Domain.Models;
using AMDI.Domain.Services;
using AMDI.Services.Excel;
using AMDI.View.Store;
using MahApps.Metro.Controls.Dialogs;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;

namespace AMDI.View.ViewModels
{
    public class ImportResultsViewModel: ViewModelBase, IRoutableViewModel
    {
        private readonly IViewStateManager stateManager;
        private readonly IMrlImportProcessor importProcessor;
        private readonly IMrlService mrlService;
        private readonly IStore store;
        private IConnectableObservable<ProcessedImportResult> resultObs;
        private IObservable<EUParsedFile> importObs;

        #region IRoutableViewModel
        public string UrlPathSegment => "import-results";
        public IScreen HostScreen { get; }
        #endregion

        #region OAPH
        readonly ObservableAsPropertyHelper<bool> loading;
        public bool Loading => loading.Value;

        readonly ObservableAsPropertyHelper<ProcessedImportResult> result;
        public ProcessedImportResult Result => result.Value;

        ObservableAsPropertyHelper<Commodity> commodity;
        public Commodity Commodity => commodity.Value;

        readonly ObservableAsPropertyHelper<List<MRL>> newRecords;
        public List<MRL> NewRecords => newRecords.Value;

        readonly ObservableAsPropertyHelper<List<MRL>> updatedRecords;
        public List<MRL> UpdatedRecords => updatedRecords.Value;

        readonly ObservableAsPropertyHelper<List<MRL>> unmatchedRecords;
        public List<MRL> UnmatchedRecords => unmatchedRecords.Value;

        readonly ObservableAsPropertyHelper<List<MRL>> unchangedRecords;
        public List<MRL> UnchangedRecords => unchangedRecords.Value;
        #endregion

        #region Reactive Commands
        private ReactiveCommand<Unit, Unit> processResultsCommand;
        public ReactiveCommand<Unit, Unit> ProcessResultsCommand => processResultsCommand;
        #endregion
        
        public ImportResultsViewModel(IScreen host, 
            IViewStateManager stateManager,
            IMrlImportProcessor importProcessor,
            IMrlService mrlService,
            IStore store)
        {
            HostScreen = host;
            this.stateManager = stateManager;
            this.importProcessor = importProcessor;
            this.mrlService = mrlService;
            this.store = store;
            importObs = this.WhenAnyObservable(x => x.stateManager.EuImport)
                .DistinctUntilChanged();

            this.resultObs = Observable.CombineLatest(importObs, stateManager.MrlProvider.DistinctUntilChanged(), (import, provider) =>
            {
                return Observable.FromAsync(async () => await this.importProcessor.ProcessImport(import, provider.ID));
            })
            .Switch()
            .Publish();


            resultObs.Select(result => result.NewRecords)
                .SubscribeOn(RxApp.TaskpoolScheduler)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.NewRecords, out newRecords, deferSubscription: true);

            resultObs.Select(result => result.UpdatedRecords)
                .SubscribeOn(RxApp.TaskpoolScheduler)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.UpdatedRecords, out updatedRecords, deferSubscription: true);

            resultObs.Select(result => result.UnmatchedRecords)
                .SubscribeOn(RxApp.TaskpoolScheduler)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.UnmatchedRecords, out unmatchedRecords, deferSubscription: true);

            resultObs.Select(result => result.UnchangedRecords)
                .SubscribeOn(RxApp.TaskpoolScheduler)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.UnchangedRecords, out unchangedRecords, deferSubscription: true);

            resultObs.Where(r => r != null)
                .SubscribeOn(RxApp.TaskpoolScheduler)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.Result, out result);

            this.WhenAnyValue(x => x.NewRecords, x => x.UnchangedRecords, x => x.UpdatedRecords, x => x.UnmatchedRecords,
                (l1, l2, l3, l4) =>
                {
                    return l1 == null
                        || l2 == null
                        || l3 == null
                        || l4 == null;
                })
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.Loading, out loading, initialValue: true);

            processResultsCommand = ReactiveCommand.CreateFromTask(async () =>
            {
                var controller = await DialogCoordinator.Instance.ShowProgressAsync(
                    this,
                    "Επεξεργασία αποτελεσμάτων",
                    "Παρακαλώ περιμένετε καθώς επεξεργαζόμαστε τα αποτελέσματα",
                    false
                    );
                controller.SetIndeterminate();
                controller.Canceled += OnControllerCanceled;

                if (await mrlService.ProcessResults(Result))
                {
                    await controller.CloseAsync();
                }
                else
                {
                    controller.SetMessage("Η επεξεργασία των αποτελεσμάτων απέτυχε!");
                    controller.SetCancelable(true);
                }

                //controller.Canceled -= OnControllerCanceled;
            });
        }

        ~ImportResultsViewModel()
        {
            this.subscriptions.Dispose();
        }

        public async void OnControllerCanceled(object sender, EventArgs args)
        {
            var controller = (ProgressDialogController)sender;

            await controller.CloseAsync();
            controller.Canceled -= OnControllerCanceled;
        }

        public void StartObservingData()
        {
            this.subscriptions.Add(this.resultObs.Connect());
            Observable.CombineLatest(importObs, store.Commodities, (import, commodities) =>
            {
                var r = commodities.FirstOrDefault(c => c.EU_ID == import.Commodity_ID);
                return r;
            })
            .Where(c => c != null)
            .ObserveOn(RxApp.MainThreadScheduler)
            .ToProperty(this, x => x.Commodity, out commodity);

            this.subscriptions.Add(importObs.Subscribe(import =>
            {
                store.LoadCommoditiesWhere(c => c.EU_ID == import.Commodity_ID);
            }));
        }
    }
}
