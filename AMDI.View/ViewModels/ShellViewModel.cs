﻿using MahApps.Metro.Controls.Dialogs;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AMDI.View.ViewModels
{
    public class ShellViewModel : ViewModelBase, IScreen
    {
        private bool _loading;
        private IDialogCoordinator dialogCoordinator;
        private readonly IViewStateManager stateManager;
        private readonly ReactiveCommand<Unit, Unit> newImportCommand;

        public bool Loading
        {
            get { return _loading; }
            set { this.RaiseAndSetIfChanged(ref _loading, value, nameof(_loading)); }
        }

        public RoutingState Router { get; set; }
        public ReactiveCommand<Unit, Unit> NewImportCommand => newImportCommand;

        public ShellViewModel(IDialogCoordinator dialogCoordinator, IViewStateManager stateManager)
        {
            this.dialogCoordinator = dialogCoordinator;
            this.stateManager = stateManager;
            this.Router = new RoutingState();
            AMDI.View.Router.Setup(this);

            newImportCommand = ReactiveCommand.CreateFromTask(async () =>
            {
                var message = (string)Application.Current.FindResource(StringResources.NewImportConfirmationMessage);
                var result = await dialogCoordinator.ShowMessageAsync(this, "Confirmation", message, MessageDialogStyle.AffirmativeAndNegative);

                if (result == MessageDialogResult.Affirmative)
                {
                    await View.Router.NavigateTo<ImportSetupViewModel>();
                }

            });

            GlobalInteractions.DisplayedToUserErrors.RegisterHandler(async interaction =>
            {
                var message = (string)Application.Current.FindResource(interaction.Input);
                await dialogCoordinator.ShowMessageAsync(this, "Error", message, MessageDialogStyle.Affirmative);

                interaction.SetOutput(Unit.Default);
            });
        }

        public void NavigateToFirstView()
        {
            AMDI.View.Router.NavigateTo<ImportSetupViewModel>();
        }
    }
}
