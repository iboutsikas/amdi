﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using AMDI.Domain.Services;
using ReactiveUI;

namespace AMDI.View.ViewModels
{
    public class IngredientSelectorViewModel: ViewModelBase
    {
        private readonly IActiveIngredientsService ingredientsService;


        #region INotifyPropertyChanged
        private string searchFilter;
        public string SearchFilter
        {
            get => searchFilter;
            set => this.RaiseAndSetIfChanged(ref searchFilter, value);
        }
        #endregion

        public IngredientSelectorViewModel(IActiveIngredientsService ingredientsService)
        {
            var textObs = this.WhenAnyValue(x => x.SearchFilter)
                            .Throttle(TimeSpan.FromMilliseconds(150))
                            .DistinctUntilChanged();
            this.ingredientsService = ingredientsService;

            //Observable.CombineLatest(textObs, this.ingredientsService.Ingredients, (txt, ingredients) =>
            //{
            //    //return ingredients.Where
            //});
        }
    }
}
