﻿using AMDI.Domain.Models;
using AMDI.Domain.Services;
using AMDI.Services.Excel;
using MahApps.Metro.Controls.Dialogs;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AMDI.View.ViewModels
{
    public class ImportSetupViewModel: ViewModelBase, IRoutableViewModel
    {
        private readonly IViewStateManager viewStateManager;
        private readonly IDialogCoordinator dialogCoordinator;
        private readonly ObservableAsPropertyHelper<bool> isProviderSelected;
        private string _selectedFilePath;

        public ProviderSelectorViewModel providerSelectorVM { get; set; }

        public IScreen HostScreen { get; set; }
        public string UrlPathSegment => "select-file";

        public bool IsProviderSelected => isProviderSelected.Value;

        
        #region Notified Props
        public string SelectedFilePath
        {
            get { return _selectedFilePath; }
            set { this.RaiseAndSetIfChanged(ref _selectedFilePath, value); }
        }

        #endregion

        #region Commands
        private ReactiveCommand<Unit, string> selectFileCommand;
        public ReactiveCommand<Unit, string> SelectFileCommand => selectFileCommand;
        private ReactiveCommand<Unit, Unit> importFileCommand;
        public ReactiveCommand<Unit, Unit> ImportFileCommand => importFileCommand;
        public IObservable<bool> CanImport;
        #endregion

        public ImportSetupViewModel(IScreen host, IViewStateManager viewStateManager,
            ProviderSelectorViewModel providerSelectorViewModel)
        {
            dialogCoordinator = DialogCoordinator.Instance;
            HostScreen = host;
            providerSelectorVM = providerSelectorViewModel;

            this.viewStateManager = viewStateManager;

            this.WhenAnyObservable(x => x.viewStateManager.MrlProvider)
                .Select(provider => provider != null && provider.ID != 0)
                .ToProperty(this, x => x.IsProviderSelected, out isProviderSelected);

            selectFileCommand = ReactiveCommand.Create(() =>
            {
                var fileDialog = new OpenFileDialog();
                var result = fileDialog.ShowDialog();

                switch (result)
                {
                    case DialogResult.OK:                        
                        SelectedFilePath = fileDialog.FileName;
                        break;
                    case DialogResult.Cancel:
                    default:
                        SelectedFilePath = null;
                        break;
                }
                return SelectedFilePath;
            });

            CanImport = this.WhenAnyValue(x => x.SelectedFilePath)
                .DistinctUntilChanged()
                .Select(value => {
                    return !string.IsNullOrEmpty(value);
                });
            importFileCommand = ReactiveCommand.CreateFromTask(async () => await OpenImportProgressDialog(SelectedFilePath), CanImport);

            importFileCommand.Subscribe(_ =>
            {
                Router.NavigateTo<ImportResultsViewModel>();
            });

            importFileCommand.ThrownExceptions
                .Throttle(TimeSpan.FromMilliseconds(250), RxApp.MainThreadScheduler)
                .Subscribe(async ex => {
                    await GlobalInteractions.DisplayedToUserErrors.Handle(StringResources.ImportFormatError);
                });

        }

        private async Task OpenImportProgressDialog(string filepath)
        {
            ProgressDialogController controller = await dialogCoordinator.ShowProgressAsync(
                this, 
                "Εισαγωγή δεδομένων", 
                String.Format("Ανάλυση αρχείου: {0}", filepath));
            controller.SetCancelable(false);

            CompositeDisposable parserSubs = new CompositeDisposable();

            IExcelParser parser = new EUExcelParser();
            try
            {
                parser.Initialize(filepath);

                parserSubs.Add(parser.OnProgress
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(percent => {
                        controller.SetProgress(percent);
                    })
                );


                var items = await parser.ParseFileAsync();
                viewStateManager.AddNewEuImport(items);

                
            }
            finally
            {
                await controller.CloseAsync();

                parserSubs.Dispose();

                parser.Dispose();
            }
        }

    }
}
