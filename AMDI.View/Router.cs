﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.View
{
    public static class Router
    {
        private static RoutingState state;
        private static IScreen screen;

        private static Subject<Unit> navigationStart = new Subject<Unit>();
        private static Subject<Unit> navigationEnd = new Subject<Unit>();

        public static IObservable<Unit> NavigationStart => navigationStart;
        public static IObservable<Unit> NavigationEnd => navigationEnd;

        public static void Setup(IScreen screen)
        {
            Router.screen = screen;
            Router.state = screen.Router;
        }

        public static IObservable<IRoutableViewModel> NavigateTo<TRoutableViewModel>() where TRoutableViewModel: IRoutableViewModel
        {
            navigationStart.OnNext(Unit.Default);

            var result = state.Navigate<TRoutableViewModel>();

            navigationEnd.OnNext(Unit.Default);
            return result;
        }
    }
}
