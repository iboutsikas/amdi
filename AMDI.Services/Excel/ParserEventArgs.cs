﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Services.Excel
{
    public class ParserProgressEventArgs: EventArgs
    {
        public int MaxRows { get; set; }
        public int CurrentRows { get; set; }
    }

    public class ParserFinishedEventArgs: EventArgs { }
}
