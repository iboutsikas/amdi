﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMDI.Services.Excel
{
    public class EUParsedFile
    {
        public int Commodity_ID { get; set; }
        public List<EUMRL> MRLs { get; set; }
    }
}
