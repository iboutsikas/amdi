﻿using System;
using System.Reactive;
using System.Threading.Tasks;

namespace AMDI.Services.Excel
{
    public interface IExcelParser: IDisposable
    {
        void Initialize(string filepath);
        IObservable<double> OnProgress { get; }
        IObservable<Unit> OnFinished { get; }
        Task<EUParsedFile> ParseFileAsync(int sheetId = 0);
        void Reset();
    }
}