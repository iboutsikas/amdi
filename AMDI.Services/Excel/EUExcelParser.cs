﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Services.Excel
{
    public class EUExcelParser: BaseParser
    {
        public EUExcelParser()
        {
        }

        public override Task<EUParsedFile> ParseFileAsync(int sheetId = 0)
        {
            if (!initialized)
            {
                throw new InvalidOperationException("Parser was not initialized. Initialize the parser with a filepath before trying to parse");
            }

            var sheet = workbook.GetSheetAt(sheetId);
            var numOfRows = sheet.PhysicalNumberOfRows;

            // Split workload
            return Task.Run(() =>
            {
                EUParsedFile result = new EUParsedFile();

                result.Commodity_ID = (int)sheet.GetRow(0).GetCell(0).NumericCellValue;
                result.MRLs = new List<EUMRL>();

                //Start from 1 since the first row was parsed to get the Commodity_ID
                for (int i = 1; i < numOfRows; i++)
                {
                    var row = sheet.GetRow(i);
                    // First cell is the ingredient id
                    int ingredient_id = (int)row.GetCell(0).NumericCellValue;

                    //Second cell is the name
                    string name = row.GetCell(1).StringCellValue;

                    // The third cell is the MRL, but it can contain strings with * or pure numbers
                    var thirdCell = row.GetCell(2);
                    var residue = GetMRLFromCell(thirdCell);

                    result.MRLs.Add(new EUMRL { EU_ID = ingredient_id, Name = name, AllowedResidue = residue });
                    double percent = ((double)i / numOfRows);
                    progressSubject.OnNext(percent);
                }

                finishedSubject.OnNext(Unit.Default);
                return result;
            });
        }

        private decimal GetMRLFromCell(ICell cell)
        {
            decimal residue = 0.0M;
            switch (cell.CellType)
            {
                case CellType.String:
                {
                    var str = cell.StringCellValue;
                    var parts = str.Split('*');
                    if (!decimal.TryParse(parts[0], out residue))
                    {
                        throw new FormatException("String in the cell was not in the proper format to be parsed.\n String received: " + str);
                    }

                    return residue;
                }
                case CellType.Numeric:
                {
                    return Convert.ToDecimal(cell.NumericCellValue);
                }
                default:
                    throw new FormatException("Cell format was neither string or numeric. Perhaps the excel file is invalid");
            }
        }
    }
}
