﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Reactive;
using System.Reactive.Subjects;

namespace AMDI.Services.Excel
{
    public abstract class BaseParser: IExcelParser
    {
        protected IWorkbook workbook;
        protected bool initialized;
        protected bool disposed;
        protected Subject<double> progressSubject;
        protected Subject<Unit> finishedSubject;

        public IObservable<double> OnProgress => progressSubject;
        public IObservable<Unit> OnFinished => finishedSubject;

        public BaseParser()
        {
            initialized = false;
            progressSubject = new Subject<double>();
            finishedSubject = new Subject<Unit>();
        }

        public void Initialize(string filepath)
        {
            EnsureCanInitialize();
            workbook = WorkbookFactory.Create(filepath);
            if (workbook != null) initialized = true;

        }

        public abstract Task<EUParsedFile> ParseFileAsync(int sheetId);

        public virtual void Reset()
        {
            EnsureCanReset();
            workbook.Close();
            workbook = null;
            initialized = false;
        }

        #region State Checks
        protected void EnsureCanInitialize()
        {
            if (initialized)
            {
                throw new InvalidOperationException("Tried to initialized a parser that was already initialized. Perhaps try reseting it first");
            }
        }

        protected void EnsureCanReset()
        {
            if (!initialized)
            {
                throw new InvalidOperationException("Tried to reset a parser that was not initialized");
            }
        }
        #endregion

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (workbook != null)
                        workbook.Close();
                }

                // Indicate that the instance has been disposed.
                disposed = true;
            }
        }
        #endregion
    }
}
