﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMDI.Services.Excel
{
    public class EUMRL
    {
        /// <summary>
        /// The ID assigned to the corresponding Active Ingredient by the EU Commission
        /// </summary>
        public int EU_ID { get; set; }

        /// <summary>
        /// The name used by the EU Commission for the corresponding active ingredient
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Maximum Residue Level defined by the EU Commission
        /// </summary>
        public decimal AllowedResidue { get; set; }

    }
}
