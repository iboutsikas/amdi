﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Data.Model
{
    public class MRL: BaseEntity
    {
        public decimal Residue { get; set; }
        public DateTime RegisteredAt { get; set; }

        public int Commodity_ID { get; set; }
        public virtual Commodity Commodity { get; set; }

        public int ActiveIngredient_ID { get; set; }
        public virtual ActiveIngredient ActiveIngredient { get; set; }

        public int Provider_ID { get; set; }
        public virtual MRLProvider Provider { get; set; }
    }
}
