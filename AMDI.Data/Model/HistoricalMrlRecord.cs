﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMDI.Data.Model
{
    public class HistoricalMrlRecord
    {
        public int ID { get; set; }
        public decimal Residue { get; set; }
        public DateTime RegisteredAt { get; set; }

        public DateTime ExpiredAt { get; set; }

        public int Commodity_ID { get; set; }
        public virtual Commodity Commodity { get; set; }

        public int ActiveIngredient_ID { get; set; }
        public virtual ActiveIngredient ActiveIngredient { get; set; }

        public int Provider_ID { get; set; }
        public virtual MRLProvider Provider { get; set; }
    }
}
