﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMDI.Data.Migrations
{
    public partial class RenameCategoryIDtoCategory_IDinActiveIngredient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveIngredients_Categories_CategoryID",
                table: "ActiveIngredients");

            migrationBuilder.DropIndex(
                name: "IX_ActiveIngredients_CategoryID",
                table: "ActiveIngredients");

            migrationBuilder.DropColumn(
                name: "CategoryID",
                table: "ActiveIngredients");

            migrationBuilder.AddColumn<int>(
                name: "Category_ID",
                table: "ActiveIngredients",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ActiveIngredients_Category_ID",
                table: "ActiveIngredients",
                column: "Category_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveIngredients_Categories_Category_ID",
                table: "ActiveIngredients",
                column: "Category_ID",
                principalTable: "Categories",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActiveIngredients_Categories_Category_ID",
                table: "ActiveIngredients");

            migrationBuilder.DropIndex(
                name: "IX_ActiveIngredients_Category_ID",
                table: "ActiveIngredients");

            migrationBuilder.DropColumn(
                name: "Category_ID",
                table: "ActiveIngredients");

            migrationBuilder.AddColumn<int>(
                name: "CategoryID",
                table: "ActiveIngredients",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActiveIngredients_CategoryID",
                table: "ActiveIngredients",
                column: "CategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_ActiveIngredients_Categories_CategoryID",
                table: "ActiveIngredients",
                column: "CategoryID",
                principalTable: "Categories",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
