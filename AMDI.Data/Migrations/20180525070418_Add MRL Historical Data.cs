﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMDI.Data.Migrations
{
    public partial class AddMRLHistoricalData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MrlHistoryRecords",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Residue = table.Column<decimal>(nullable: false),
                    RegisteredAt = table.Column<DateTime>(nullable: false),
                    ExpiredAt = table.Column<DateTime>(nullable: false),
                    Commodity_ID = table.Column<int>(nullable: false),
                    ActiveIngredient_ID = table.Column<int>(nullable: false),
                    Provider_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MrlHistoryRecords", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MrlHistoryRecords_ActiveIngredients_ActiveIngredient_ID",
                        column: x => x.ActiveIngredient_ID,
                        principalTable: "ActiveIngredients",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MrlHistoryRecords_Commodities_Commodity_ID",
                        column: x => x.Commodity_ID,
                        principalTable: "Commodities",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MrlHistoryRecords_Providers_Provider_ID",
                        column: x => x.Provider_ID,
                        principalTable: "Providers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MrlHistoryRecords_ActiveIngredient_ID",
                table: "MrlHistoryRecords",
                column: "ActiveIngredient_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MrlHistoryRecords_Commodity_ID",
                table: "MrlHistoryRecords",
                column: "Commodity_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MrlHistoryRecords_Provider_ID",
                table: "MrlHistoryRecords",
                column: "Provider_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MrlHistoryRecords");
        }
    }
}
