﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace AMDI.Data.Context
{
    public interface IDesignTimeDbContextFactoryBase<TContext>: IDesignTimeDbContextFactory<TContext> where TContext : DbContext
    {
        TContext Create();
    }
}