﻿using AMDI.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.Data.Context
{
    public class AMDIContext: DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<ActiveIngredient> ActiveIngredients { get; set; }
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<MRL> MRLs { get; set; }
        public DbSet<MRLProvider> Providers { get; set; }
        public DbSet<HistoricalMrlRecord> MrlHistoryRecords { get; set; }

        public AMDIContext(DbContextOptions options)
            :base(options)
        {

        }

        public AMDIContext()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ActiveIngredient>()
                .HasOne(a => a.Category)
                .WithMany()
                .HasForeignKey(a => a.Category_ID);

            modelBuilder.Entity<MRL>()
                .HasOne(m => m.ActiveIngredient)
                .WithMany()
                .HasForeignKey(m => m.ActiveIngredient_ID);

            modelBuilder.Entity<MRL>()
                .HasOne(m => m.Commodity)
                .WithMany()
                .HasForeignKey(m => m.Commodity_ID);

            modelBuilder.Entity<MRL>()
                .HasOne(m => m.Provider)
                .WithMany()
                .HasForeignKey(m => m.Provider_ID);

            modelBuilder.Entity<HistoricalMrlRecord>()
                .HasOne(m => m.ActiveIngredient)
                .WithMany()
                .HasForeignKey(m => m.ActiveIngredient_ID);

            modelBuilder.Entity<HistoricalMrlRecord>()
                .HasOne(m => m.Commodity)
                .WithMany()
                .HasForeignKey(m => m.Commodity_ID);

            modelBuilder.Entity<HistoricalMrlRecord>()
                .HasOne(m => m.Provider)
                .WithMany()
                .HasForeignKey(m => m.Provider_ID);

            var auditables = modelBuilder.Model.GetEntityTypes()
                    .Where(e => e.ClrType.IsSubclassOf(typeof(BaseEntity)));

            foreach (var entityType in auditables)
            {
                modelBuilder
                    .Entity(entityType.ClrType)
                    .Property("CreatedAt")
                    .HasDefaultValueSql("getutcdate()");

                modelBuilder
                    .Entity(entityType.ClrType)
                    .Property("ModifiedAt")
                    .HasDefaultValueSql("getutcdate()");
            }
        }

        public override int SaveChanges()
        {
            AddAuitInfo();
            return base.SaveChanges();
        }

        public Task SaveChangesAsync()
        {
            AddAuitInfo();
            return base.SaveChangesAsync();
        }

        private void AddAuitInfo()
        {
            var entries = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));
            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Added)
                {
                    ((BaseEntity)entry.Entity).CreatedAt = DateTime.UtcNow;
                }
            ((BaseEntity)entry.Entity).ModifiedAt = DateTime.UtcNow;
            }
        }
    }
}
