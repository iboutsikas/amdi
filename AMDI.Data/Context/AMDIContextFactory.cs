﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMDI.Data.Context
{
    public class AMDIContextFactory: DesignTimeDbContextFactoryBase<AMDIContext>
    {
        private static AMDIContextFactory _instance;
        public static AMDIContextFactory Instance => _instance ?? new AMDIContextFactory();


        protected override AMDIContext CreateNewInstance(DbContextOptions<AMDIContext> options)
        {
            return new AMDIContext(options);
        }
    }
}
