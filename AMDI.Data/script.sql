/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2017 (14.0.1000)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/
USE [excel_test]
GO
DELETE FROM [dbo].[MRLs]
GO
DELETE FROM [dbo].[Providers]
GO
DELETE FROM [dbo].[Commodities]
GO
DELETE FROM [dbo].[ActiveIngredients]
GO
DELETE FROM [dbo].[Categories]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([ID], [Name], [Shorthand]) VALUES (1, N'Εντομοκτόνα', N'ΕΝ')
INSERT [dbo].[Categories] ([ID], [Name], [Shorthand]) VALUES (2, N'Αφιδοκτόνα', N'ΑΦ')
INSERT [dbo].[Categories] ([ID], [Name], [Shorthand]) VALUES (3, N'Ακαρεοκτόνα', N'ΑΚ')
INSERT [dbo].[Categories] ([ID], [Name], [Shorthand]) VALUES (4, N'Μυκητοκτόνα', N'ΜΥΚ')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[ActiveIngredients] ON 

INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (1, N'Abamectin', 8, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (2, N'Acetamiprid', 10, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (3, N'Bifenazate', 27, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (4, N'Captan', 36, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (5, N'Chlorothalonil', 53, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (6, N'Chlorpyrifos', 56, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (7, N'Chlorpyrifos-methyl', 57, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (8, N'Clofentezine', 60, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (9, N'Beta cyfluthrin', 63, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (10, N'Deltamethrin', 69, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (11, N'Mancozeb', 83, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (12, N'Metiram', 83, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (13, N'Etoxazole', 92, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (14, N'Fenhexamid', 99, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (15, N'Indoxacarb', 132, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (16, N'Iprodione', 135, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (17, N'Kresoxim methyl', 139, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (18, N'Lambda cyhalothrin', 140, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (19, N'Methoxyfenozide', 159, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (20, N'Milbemectin', 162, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (21, N'Myclobutanil', 166, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (22, N'Penconazole', 175, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (23, N'Prochloraz', 184, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (24, N'Propiconazole', 188, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (25, N'Propineb', 189, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (26, N'Pymetrozine', 194, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (27, N'Pyraclostrobin', 195, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (28, N'Pyrimethanil', 199, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (29, N'Quinoxyfen', 201, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (30, N'Thiacloprid', 211, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (31, N'Thiophanate methyl', 213, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (32, N'Thiram', 214, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (33, N'Trifloxystrobin', 221, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (34, N'Ziram', 226, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (35, N'Acequinocyl', 231, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (36, N'Acrinathrin', 234, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (37, N'Azadirachtin', 239, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (38, N'Boscalid', 244, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (39, N'Bupirimate', 247, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (40, N'Copper', 262, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (41, N'Cyproconazole', 266, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (42, N'Cyprodinil', 267, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (43, N'Difenoconazole', 276, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (44, N'Diflubenzuron', 277, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (45, N'Dithianon', 285, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (46, N'Dodine', 287, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (47, N'Etofenprox', 294, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (48, N'Fenbuconazole', 297, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (49, N'Fenoxycarb', 299, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (50, N'Fenpyroximate', 301, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (51, N'Fludioxonil', 307, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (52, N'Fluquinconazole', 311, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (53, N'Flutriafol', 315, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (54, N'Formetanate', 316, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (55, N'Fosetyl', 317, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (56, N'Hexythiazox', 323, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (57, N'Imidacloprid', 326, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (58, N'Phosmet', 353, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (59, N'Pyrethrins', 366, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (60, N'Pyriproxyfen', 368, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (61, N'Spinosad', 373, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (62, N'Spirodiclofen', 374, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (63, N'Tau fluvalinate', 378, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (64, N'Tebuconazole', 379, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (65, N'Tebufenozide', 380, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (66, N'Tebufenpyrad', 381, 3)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (67, N'Tetraconazole', 386, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (68, N'Thiamethoxam', 388, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (69, N'Chlorantraniliprole', 771, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (70, N'Clothianidin', 775, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (71, N'Cyflufenamid', 776, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (72, N'Flonicamid', 778, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (73, N'Spinetoram', 805, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (74, N'Spirotetramat', 806, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (75, N'Emamectin benzoate', 2037, 1)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (76, N'Isopyrazam', 2168, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (77, N'Fluxapyroxad', 2268, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (78, N'Fluopyram', 2276, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (79, N'Fenpyrazamine', 2281, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (80, N'Penthiopyrad', 2302, 4)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (81, N'Sulfoxaflor', 2370, 2)
INSERT [dbo].[ActiveIngredients] ([ID], [Name], [EU_ID], [Category_ID]) VALUES (82, N'Cyflumetofen', 2377, 3)
SET IDENTITY_INSERT [dbo].[ActiveIngredients] OFF
SET IDENTITY_INSERT [dbo].[Commodities] ON 

INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (83, N'Peaches', 140030, N'Peaches')
INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (84, N'Cherries', 140020, N'Cherries (sweet)')
INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (85, N'Plums', 140040, N'Plums')
INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (86, N'Apricots', 140010, N'Apricots')
INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (87, N'Apples', 130010, N'Apples')
INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (88, N'Pears', 130020, N'Pears')
INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (89, N'Quinces', 130030, N'Quinces')
INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (90, N'Kiwis', 162010, N'Kiwi fruits (green, red, yellow)')
INSERT [dbo].[Commodities] ([ID], [Name], [EU_ID], [EU_Name]) VALUES (91, N'Persimmons', 161060, N'Kaki/Japanese persimmons')
SET IDENTITY_INSERT [dbo].[Commodities] OFF
SET IDENTITY_INSERT [dbo].[Providers] ON 

INSERT [dbo].[Providers] ([ID], [Name], [Logo]) VALUES (1, N'European Commision', N'european_commission.jpg')
SET IDENTITY_INSERT [dbo].[Providers] OFF
SET IDENTITY_INSERT [dbo].[MRLs] ON 
