﻿using AMDI.API.Features.ActiveIngredient.Commands;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AMDI.API.Tests.Features.ActiveIngredient
{
    public class DeleteIngredientCommandTests: TestBase
    {
        public DeleteIngredientCommandTests()
            :base()
        {

        }

        [Fact]
        public async Task DeletesTheActiveIngredient()
        {
            var random = new Random();

            var id = random.Next(1, SeedData.ActiveIngredients.Count);

            using (var context = GetContext())
            {
                var command = new DeleteIngredientCommand(context);
                await command.Execute(id);

                var actual = await context.ActiveIngredients
                    .FirstOrDefaultAsync(a => a.ID == id);

                Assert.Null(actual);

            }
        }

        [Fact]
        public async Task GivenInvalidIdThrows()
        {
            var random = new Random();

            var id = SeedData.ActiveIngredients.Count + 1;

            using (var context = GetContext())
            {
                var command = new DeleteIngredientCommand(context);
                await Assert.ThrowsAsync<DbUpdateConcurrencyException>(() => command.Execute(id));
            }
        }
    }
}
