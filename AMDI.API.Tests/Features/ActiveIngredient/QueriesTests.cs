﻿using AMDI.API.Features;
using AMDI.API.Features.ActiveIngredient;
using AMDI.Data.Context;
using DomainModel = AMDI.Domain.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using System.Collections.Generic;

namespace AMDI.API.Tests.Features.ActiveIngredient
{
    public class QueriesTests: TestBase
    {
        public QueriesTests()
            :base()
        {
            
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        [InlineData(8)]
        public async Task GivenValidID_QueryReturnsTheCorrespondingObject(int id)
        {
            using (var context = GetContext())
            {
                IQuery<DomainModel.ActiveIngredient> query = new ListSingleIngredient(context, id);

                var actual = await query.Execute();
                var expected = SeedData.ActiveIngredients[id - 1];
                Assert.Equal(expected.Name, actual.Name);
                Assert.Equal(expected.EU_ID, actual.EU_ID);
            }
        }

        [Fact]
        public async Task GivenInvalidID_QueryReturnsNull()
        {
            using (var context = GetContext())
            {
                IQuery<DomainModel.ActiveIngredient> query = new ListSingleIngredient(context, SeedData.ActiveIngredients.Count + 1);

                var actual = await query.Execute();

                Assert.Null(actual);
            }
        }

        [Fact]
        public async Task GivenNoPageData_QueryReturnsAll()
        {
            using (var context = GetContext())
            {
                IQuery<List<DomainModel.ActiveIngredient>> query = new ListIngredientsQuery(context);

                var actual = await query.Execute();

                Assert.Equal(SeedData.ActiveIngredients.Count, actual.Count);
            }
        }

        [Theory]
        [InlineData(1, 4)]
        [InlineData(2, 4)]
        public async Task GivenPageData_QueryReturnsMatching(int page, int pageSize)
        {
            using (var context = GetContext())
            {
                IQuery<List<DomainModel.ActiveIngredient>> query = new ListIngredientsQuery(context, page, pageSize);

                var actual = await query.Execute();
                var expected = SeedData.ActiveIngredients.AsQueryable()
                    .Skip(page * pageSize)
                    .Take(pageSize);

                Assert.Equal(expected.Count(), actual.Count);

                for (int i = 0; i < actual.Count; i++)
                {
                    Assert.Equal(expected.ElementAt(i).Name, actual[i].Name);
                }
            }
        }

        [Fact]
        public async Task GivenFilter_QueryReturnsMatching()
        {
            string name = "Abamectin";

            using (var context = GetContext())
            {
                IQuery<List<DomainModel.ActiveIngredient>> query = new ListIngredientsQuery(context, filter: a => a.Name == name);

                var actual = await query.Execute();

                Assert.Single(actual);
                Assert.Equal(name, actual[0].Name);
            }
        }
    }
}
