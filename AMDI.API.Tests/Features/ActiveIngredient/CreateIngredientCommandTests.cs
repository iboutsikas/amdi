﻿using AMDI.API.Features.ActiveIngredient.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DomainModel = AMDI.Domain.Models;
using DataModel = AMDI.Data.Model;
using Xunit;
using Microsoft.EntityFrameworkCore;

namespace AMDI.API.Tests.Features.ActiveIngredient
{
    public class CreateIngredientCommandTests: TestBase
    {
        public CreateIngredientCommandTests()
            :base()
        {

        }
        [Fact]
        public async Task SuccessfullyCreatesAnIngredient()
        {
            var random = new Random();
            string name = "Superfarmako";
            int eu_id = 1821;
            int category_index = random.Next(0, SeedData.Categories.Count);
            var cat = SeedData.Categories[category_index];

            using (var context = GetContext())
            {
                var command = new CreateIngredientCommand(context);
                var result = await command.Execute(name, eu_id, cat.ID);

                Assert.Equal(name, result.Name);
                Assert.Equal(eu_id, result.EU_ID);
                Assert.Equal(cat.ID, result.Category_ID);
                Assert.Equal(cat.Name, result.Category);
            }
        }

        [Fact]
        public async Task WhenGivenInvalidCategoryID_ThrowsUpdateException()
        {
            //var random = new Random();
            string name = "Superfarmako";
            int eu_id = 1821;
            //int category_index = random.Next(0, SeedData.Categories.Count - 1);
            //var cat = SeedData.Categories[category_index];

            using (var context = GetContext())
            {
                var command = new CreateIngredientCommand(context);
                await Assert.ThrowsAsync<DbUpdateException>(() => command.Execute(name, eu_id, 1821));
            }
        }
    }
}
