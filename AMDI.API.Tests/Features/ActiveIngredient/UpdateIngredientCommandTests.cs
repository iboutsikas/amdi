﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataModel = AMDI.Data.Model;
using DomainModel = AMDI.Domain.Models;
using AMDI.API.Features.ActiveIngredient.ViewModels;
using AMDI.API.Features.ActiveIngredient.Commands;
using Xunit;

namespace AMDI.API.Tests.Features.ActiveIngredient
{
    public class UpdateIngredientCommandTests: TestBase
    {
        private DataModel.ActiveIngredient original;

        public UpdateIngredientCommandTests()
            :base()
        {
            var random = new Random();
            var index = random.Next(0, SeedData.ActiveIngredients.Count);
            original = SeedData.ActiveIngredients[index];
        }

        [Fact]
        public async Task UpdatesOnlyTheIngredientName()
        {
            var vm = new PutViewModel()
            {
                ID = original.ID,
                Name = "Potatofarmako"
            };

            using (var context = GetContext())
            {
                var command = new UpdateIngredientCommand(context, vm);
                var result = await command.Execute();

                Assert.Equal(original.ID, result.ID);
                Assert.Equal(vm.Name, result.Name);
                Assert.Equal(original.EU_ID, result.EU_ID);
                Assert.Equal(original.Category_ID, result.Category_ID);
            }
        }

        [Fact]
        public async Task UpdatesOnlyTheEUID()
        {
            var vm = new PutViewModel()
            {
                ID = original.ID,
                EU_ID = 19929288
            };

            using (var context = GetContext())
            {
                var command = new UpdateIngredientCommand(context, vm);
                var result = await command.Execute();

                Assert.Equal(original.ID, result.ID);
                Assert.Equal(original.Name, result.Name);
                Assert.Equal(vm.EU_ID, result.EU_ID);
                Assert.Equal(original.Category_ID, result.Category_ID);
            }
        }

        [Fact]
        public async Task UpdatesOnlyTheCategoryID()
        {
            var random = new Random();
            DataModel.Category category;
            do
            {
                var cat_index = random.Next(0, SeedData.Categories.Count);
                category = SeedData.Categories[cat_index];
            } while (category.ID == original.Category_ID);

            var vm = new PutViewModel()
            {
                ID = original.ID,
                Category_ID = category.ID
            };

            using (var context = GetContext())
            {
                var command = new UpdateIngredientCommand(context, vm);
                var result = await command.Execute();

                Assert.Equal(original.ID, result.ID);
                Assert.Equal(original.Name, result.Name);
                Assert.Equal(original.EU_ID, result.EU_ID);
                Assert.Equal(vm.Category_ID, result.Category_ID);
            }
        }

        [Fact]
        public async Task UpdatesTheWholeIngredient()
        {
            var random = new Random();
            DataModel.Category category;
            do
            {
                var cat_index = random.Next(0, SeedData.Categories.Count);
                category = SeedData.Categories[cat_index];
            } while (category.ID == original.Category_ID);

            var vm = new PutViewModel()
            {
                ID = original.ID,
                Name = "Superfarmako",
                Category_ID = category.ID,
                EU_ID = 58889
            };

            using (var context = GetContext())
            {
                var command = new UpdateIngredientCommand(context, vm);
                var result = await command.Execute();

                Assert.Equal(original.ID, result.ID);
                Assert.Equal(vm.Name, result.Name);
                Assert.Equal(vm.EU_ID, result.EU_ID);
                Assert.Equal(vm.Category_ID, result.Category_ID);
            }
        }

        [Fact]
        public async Task InvalidIDReturnsNull()
        {
            using (var context = GetContext())
            {
                var vm = new PutViewModel
                {
                    ID = SeedData.ActiveIngredients.Count + 1
                };

                var command = new UpdateIngredientCommand(context, vm);

                var result = await command.Execute();

                Assert.Null(result);
            }
        }
    }
}
