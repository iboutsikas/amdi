﻿using AMDI.Data.Context;
using AMDI.Data.Model;
using AMDI.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMDI.API.Tests
{
    public static class ContextExtensions
    {
        private static bool isMapperInitialized = false;
        public static void SeedTestDatabase(this AMDIContext context)
        {
            context.Categories.AddRange(SeedData.Categories);
            context.SaveChanges();

            context.ActiveIngredients.AddRange(SeedData.ActiveIngredients);
            context.SaveChanges();

            context.Commodities.AddRange(SeedData.Commodities);
            context.SaveChanges();

            if (!isMapperInitialized)
            {
                DomainMapper.Initialize();
                isMapperInitialized = true;
            }
        }
    }

    public static class SeedData
    {
        public static List<Category> Categories = new List<Category>
        {
            new Category { Name = "Εντομοκτόνα",    Shorthand = "ΕΝ" },
            new Category { Name = "Αφιδοκτόνα",     Shorthand = "ΑΦ" },
            new Category { Name = "Ακαρεοκτόνα",    Shorthand = "ΑΚ" },
            new Category { Name = "Μυκητοκτόνα",    Shorthand = "ΜΥΚ" }
        };

        public static List<ActiveIngredient> ActiveIngredients = new List<ActiveIngredient>
        {
            new ActiveIngredient { Name = "Abamectin",              EU_ID = 8,      Category_ID = 1 },
            new ActiveIngredient { Name = "Acetamiprid",            EU_ID = 10,     Category_ID = 2 },
            new ActiveIngredient { Name = "Bifenazate",             EU_ID = 27,     Category_ID = 3 },
            new ActiveIngredient { Name = "Captan",                 EU_ID = 36,     Category_ID = 4 },
            new ActiveIngredient { Name = "Chlorothalonil",         EU_ID = 53,     Category_ID = 4 },
            new ActiveIngredient { Name = "Chlorpyrifos",           EU_ID = 56,     Category_ID = 1 },
            new ActiveIngredient { Name = "Chlorpyrifos-methyl",    EU_ID = 57,     Category_ID = 1 },
            new ActiveIngredient { Name = "Clofentezine",           EU_ID = 60,     Category_ID = 3 },
            new ActiveIngredient { Name = "Beta cyfluthrin",        EU_ID = 63,     Category_ID = 1 },
            new ActiveIngredient { Name = "Deltamethrin",           EU_ID = 69,     Category_ID = 1 },
            new ActiveIngredient { Name = "Mancozeb",               EU_ID = 83,     Category_ID = 4 },
        };

        public static List<Commodity> Commodities = new List<Commodity>
        {
            new Commodity { Name = "Peaches",   EU_ID = 140030 },
            new Commodity { Name = "Cherries",  EU_ID = 140020 },
            new Commodity { Name = "Plums",     EU_ID = 140040 },
            new Commodity { Name = "Apricots",  EU_ID = 140010 },
            new Commodity { Name = "Apples",    EU_ID = 130010 },
            new Commodity { Name = "Pears",     EU_ID = 130020 },
            new Commodity { Name = "Quinces",   EU_ID = 130030 }
        };
    }
}
