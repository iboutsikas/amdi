﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Models
{
    public class ActiveIngredient
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        [JsonProperty(PropertyName = "category_id")]
        public int Category_ID { get; set; }
        [JsonProperty(PropertyName = "eu_id")]
        public int EU_ID { get; set; }
    }
}
