﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Models
{
    public class Commodity: IEquatable<Commodity>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        [JsonProperty(PropertyName = "eu_id")]
        public int EU_ID { get; set; }

        public bool Equals(Commodity other)
        {
            return other != null &&
                   ID == other.ID;
        }

        public override bool Equals(object obj)
        {
            if (obj is Commodity)
                return this.Equals(obj as Commodity);
            return false;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = (int)2166136261;
                // Suitable nullity checks etc, of course :)
                hash = (hash * 16777619) ^ ID.GetHashCode();
                hash = (hash * 16777619) ^ EU_ID.GetHashCode();
                return hash;
            }
        }

        public static bool operator ==(Commodity commodity1, Commodity commodity2)
        {
            return EqualityComparer<Commodity>.Default.Equals(commodity1, commodity2);
        }

        public static bool operator !=(Commodity commodity1, Commodity commodity2)
        {
            return !(commodity1 == commodity2);
        }
    }
}
