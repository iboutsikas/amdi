﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Models
{
    public class MRL
    {
        public int ID { get; set; }
        [JsonProperty(PropertyName = "residue")]
        public decimal AllowedResidue { get; set; }
        [JsonIgnore]
        public decimal OldAllowedResidue { get; set; }
        [JsonProperty(PropertyName = "ingredient_id")]
        public int IngredientID { get; set; }
        [JsonProperty(PropertyName = "ingredient_name")]
        public string IngredientName { get; set; }
        [JsonProperty(PropertyName = "commodity_id")]
        public int CommodityID { get; set; }
        [JsonProperty(PropertyName = "commodity_name")]
        public string CommodityName { get; set; }
        [JsonIgnore]
        public int ProviderID { get; set; }
        [JsonProperty(PropertyName = "registered_at")]
        public DateTime RegisteredAt { get; set; }
        [JsonProperty(PropertyName = "expired_at")]
        public DateTime? ExpiredAt { get; set; } = null;

        [JsonIgnore]
        public string Display => $"{IngredientName} - {AllowedResidue}";
    }
}
