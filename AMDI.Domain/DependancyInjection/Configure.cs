﻿using AMDI.Domain.Services;
using DryIoc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.DependancyInjection
{
    public static class Configure
    {
        public static IServiceCollection AddDomainLayer(this IServiceCollection services)
        {
            services.AddScoped<IMrlProviderService, PerRequestMrlProviderService>();
            services.AddScoped<IActiveIngredientsService, PerRequestActiveIngredientsService>();
            services.AddScoped<ICommoditiesService, PerRequestCommoditiesService>();

            return services;
        }

        public static void AddDomain(this Container container)
        {
            container.Register<IMrlProviderService, MrlProviderService>(Reuse.Singleton);
            container.Register<IActiveIngredientsService, ActiveIngredientsService>(Reuse.Singleton);
            container.Register<ICommoditiesService, CommoditiesService>(Reuse.Singleton);
            container.Register<IMrlImportProcessor, MrlImportProcessor>();
            container.Register<IMrlService, MrlService>();
        }
    }
}
