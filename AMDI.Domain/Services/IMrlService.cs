﻿using System.Threading.Tasks;

namespace AMDI.Domain.Services
{
    public interface IMrlService
    {
        Task<bool> ProcessResults(ProcessedImportResult importResult);
    }
}