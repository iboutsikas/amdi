﻿using AMDI.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DryIoc;
using AMDI.Data.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AMDI.Domain.Services
{
    public class MrlProviderService : IMrlProviderService
    {
        private readonly Container container;

        public MrlProviderService(Container container)
        {
            this.container = container;
        }

        public async Task<List<MrlProvider>> GetAll()
        {
            var db = container.Resolve<AMDIContext>();

            var items = await db.Providers
                .AsNoTracking()
                .Select(p => Mapper.Map<MrlProvider>(p))
                .ToListAsync();

            db.Dispose();

            items.Add(MrlProvider.NYI_Provider);

                return items;
        }
    }
}
