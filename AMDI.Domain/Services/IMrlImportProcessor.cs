﻿using System.Threading.Tasks;
using AMDI.Services.Excel;

namespace AMDI.Domain.Services
{
    public interface IMrlImportProcessor
    {
        Task<ProcessedImportResult> ProcessImport(EUParsedFile parsedFile, int providerID);
    }
}