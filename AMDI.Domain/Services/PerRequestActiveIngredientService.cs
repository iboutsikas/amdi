﻿using AMDI.Data.Context;
using AMDI.Domain.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.Domain.Services
{
    public class PerRequestActiveIngredientsService: IActiveIngredientsService
    {
        private readonly AMDIContext context;
        public PerRequestActiveIngredientsService(AMDIContext context)
        {
            this.context = context;
        }

        public Task<List<ActiveIngredient>> GetAll()
        {
            return context.ActiveIngredients
                .Include(a => a.Category)
                .Select(ai => Mapper.Map<ActiveIngredient>(ai))
                .ToListAsync();
        }
    }
}
