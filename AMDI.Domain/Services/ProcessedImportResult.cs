﻿using AMDI.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Services
{
    public class ProcessedImportResult
    {
        public List<MRL> NewRecords { get; set; }
        public List<MRL> UpdatedRecords { get; set; }
        public List<MRL> UnchangedRecords { get; set; }
        public List<MRL> UnmatchedRecords { get; set; }

        public ProcessedImportResult()
        {
            NewRecords = new List<MRL>();
            UpdatedRecords = new List<MRL>();
            UnchangedRecords = new List<MRL>();
            UnmatchedRecords = new List<MRL>();
        }

    }
}
