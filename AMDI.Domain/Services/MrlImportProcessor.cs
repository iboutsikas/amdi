﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMDI.Data.Context;
using AMDI.Domain.Exceptions;
using AMDI.Domain.Models;
using AMDI.Services.Excel;
using DryIoc;

namespace AMDI.Domain.Services
{
    public class MrlImportProcessor: IMrlImportProcessor
    {
        private readonly Container container;

        public MrlImportProcessor(Container container)
        {
            this.container = container;
        }
        public Task<ProcessedImportResult> ProcessImport(EUParsedFile parsedFile, int providerID)
        {
            return Task.Run(() =>
            {
                using (var db = container.Resolve<AMDIContext>())
                {
                    var commodity = db.Commodities.SingleOrDefault(c => c.EU_ID == parsedFile.Commodity_ID);

                    if (commodity == null)
                    {
                        throw new UnmatchedCommodityException();
                    }

                    var result = new ProcessedImportResult();

                    List<Data.Model.ActiveIngredient> allIngredients = db.ActiveIngredients.ToList();

                    foreach (var mrl in parsedFile.MRLs)
                    {
                        // These are all the ignredients with this ID. Multiple ingredients can share an id
                        // so we need to create an MRL record for each of them
                        var ingredients = allIngredients
                            .Where(ai => ai.EU_ID == mrl.EU_ID);

                        // If there are no ingredients, we are not configured to track this ActiveIngredient
                        if (ingredients.Count() == 0)
                            result.UnmatchedRecords.Add(new MRL { AllowedResidue = mrl.AllowedResidue, IngredientName = mrl.Name, CommodityID = commodity.ID });
                        else
                        {
                            foreach (var ing in ingredients)
                            {
                                var existingMrl = db.MRLs.Where(m => m.ActiveIngredient_ID == ing.ID
                                                                        && m.Commodity_ID == commodity.ID
                                                                        && m.Provider_ID == providerID
                                                                    ).FirstOrDefault();
                                // New record, no previous history
                                if (existingMrl == null)
                                {
                                    result.NewRecords.Add(new MRL {
                                        AllowedResidue = mrl.AllowedResidue,
                                        IngredientName = ing.Name,
                                        IngredientID = ing.ID,
                                        CommodityID = commodity.ID
                                    });
                                }
                                // Updated record. There is an existing record but with different residue
                                else if (mrl.AllowedResidue != existingMrl.Residue)
                                {
                                    result.UpdatedRecords.Add(new MRL {
                                        ID = existingMrl.ID,
                                        AllowedResidue = mrl.AllowedResidue,
                                        OldAllowedResidue = existingMrl.Residue,
                                        RegisteredAt = existingMrl.RegisteredAt,
                                        IngredientName = ing.Name,
                                        IngredientID = ing.ID,
                                        CommodityID = commodity.ID });
                                }
                                // Existing record, nothing to update
                                else
                                {
                                    result.UnchangedRecords.Add(new MRL { AllowedResidue = mrl.AllowedResidue, IngredientID = ing.ID, IngredientName = ing.Name });
                                }

                            }
                        }
                    }

                    return result;
                }
            });            
        }
    }
}
