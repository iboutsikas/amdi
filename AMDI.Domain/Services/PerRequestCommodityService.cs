﻿using AMDI.Data.Context;
using AMDI.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace AMDI.Domain.Services
{
    public class PerRequestCommoditiesService: ICommoditiesService
    {
        private readonly AMDIContext context;

        public PerRequestCommoditiesService(AMDIContext context)
        {
            this.context = context;
        }

        public async Task<HashSet<Commodity>> GetAll()
        {
            var items = await context.Commodities
                .Select(c => Mapper.Map<Commodity>(c))
                .ToListAsync();

            return new HashSet<Commodity>(items);
        }

        public async Task<List<Commodity>> GetWhere(Expression<Func<Commodity, bool>> filter)
        {
            var result = await context.Commodities
                .Select(c => Mapper.Map<Commodity>(c))
                .Where(filter)
                .ToListAsync();

            return result;
        }

    }
}
