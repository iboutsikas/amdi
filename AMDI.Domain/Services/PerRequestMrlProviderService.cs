﻿using AMDI.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMDI.Data.Context;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace AMDI.Domain.Services
{
    public class PerRequestMrlProviderService : IMrlProviderService
    {
        private readonly AMDIContext context;

        public PerRequestMrlProviderService(AMDIContext context)
        {
            this.context = context;
        }

        public Task<List<MrlProvider>> GetAll()
        {
            return context.Providers
                .Select(p => Mapper.Map<MrlProvider>(p))
                .ToListAsync();
        }
    }
}
