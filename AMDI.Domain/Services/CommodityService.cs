﻿using AMDI.Data.Context;
using AMDI.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using DryIoc;

namespace AMDI.Domain.Services
{
    public class CommoditiesService: ICommoditiesService
    {
        private readonly Container container;

        public CommoditiesService(Container container)
        {
            this.container = container;
        }

        public async Task<HashSet<Commodity>> GetAll()
        {
            using (var db = container.Resolve<AMDIContext>())
            {
                var items = await db.Commodities
                    .Select(c => Mapper.Map<Commodity>(c))
                    .ToListAsync();
                return new HashSet<Commodity>(items);
            }
        }

        public async Task<List<Commodity>> GetWhere(Expression<Func<Commodity, bool>> filter)
        {
            using (var db = container.Resolve<AMDIContext>())
            {
                var result = await db.Commodities
                    .Select(c => Mapper.Map<Commodity>(c))
                    .Where(filter)
                    .ToListAsync();

                return result;
            }
        }

    }
}
