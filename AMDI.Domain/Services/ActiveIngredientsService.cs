﻿using AMDI.Data.Context;
using AMDI.Domain.Models;
using AutoMapper;
using DryIoc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMDI.Domain.Services
{
    public class ActiveIngredientsService: IActiveIngredientsService
    {
        private readonly Container container;

        public ActiveIngredientsService(Container container)
        {
            this.container = container;
        }

        public Task<List<ActiveIngredient>> GetAll()
        {
            using (var db = container.Resolve<AMDIContext>())
            {
                return db.ActiveIngredients
                    .Select(ai => Mapper.Map<ActiveIngredient>(ai))
                    .ToListAsync();
            }
        }
    }
}
