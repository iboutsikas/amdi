﻿using AMDI.Data.Context;
using AMDI.Domain.Models;
using DryIoc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain.Services
{
    public class MrlService: IMrlService
    {
        private readonly Container container;

        public MrlService(Container container)
        {
            this.container = container;
        }

        public async Task<bool> ProcessResults(ProcessedImportResult importResult)
        {
            using (var context = container.Resolve<AMDIContext>())
            {
                var transaction = await context.Database.BeginTransactionAsync();
                try
                {
                    await handleNewResults(importResult.NewRecords, context);
                    await handleUpdatedResults(importResult.UpdatedRecords, context);

                    transaction.Commit();
                    return true;
                }
                catch(Exception e)
                {
                    transaction.Rollback();
                    return false;
                }
                finally
                {
                    transaction.Dispose();
                }
            }
        }

        private async Task handleNewResults(List<MRL> mrls, AMDIContext context)
        {
            foreach (var m in mrls)
            {
                var mrl = new Data.Model.MRL
                {
                    Residue = m.AllowedResidue,
                    ActiveIngredient_ID = m.IngredientID,
                    Commodity_ID = m.CommodityID,
                    Provider_ID = 1,
                    RegisteredAt = DateTime.UtcNow
                };

                await context.MRLs.AddAsync(mrl);
            }

            await context.SaveChangesAsync();
        }

        private async Task handleUpdatedResults(List<MRL> mrls, AMDIContext context)
        {
            foreach (var m in mrls)
            {
                // Modify the existing record
                var mrl = new Data.Model.MRL
                {
                    ID = m.ID
                };

                context.Attach(mrl);

                mrl.Residue = m.AllowedResidue;
                mrl.ModifiedAt = DateTime.UtcNow;

                //Create historical record
                var history = new Data.Model.HistoricalMrlRecord
                {
                    ActiveIngredient_ID = m.IngredientID,
                    Commodity_ID = m.CommodityID,
                    Provider_ID = 1,
                    ExpiredAt = DateTime.UtcNow,
                    Residue = m.OldAllowedResidue,
                    RegisteredAt = m.RegisteredAt
                };

                await context.AddAsync(history);
            }

            await context.SaveChangesAsync();
        }
    }
}
