﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMDI.Domain
{
    public static class DomainMapper
    {
        private static bool initialized = false;

        public static void Initialize()
        {
            if (initialized) return;

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Data.Model.ActiveIngredient, Models.ActiveIngredient>()
                    .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category.Name))
                    .ForMember(dest => dest.Category_ID, opt => opt.MapFrom(src => src.Category.ID));

                cfg.CreateMap<Data.Model.Commodity, Models.Commodity>();

                cfg.CreateMap<Data.Model.Category, Models.Category>();

                cfg.CreateMap<Data.Model.MRL, Models.MRL>()
                    .ForMember(dest => dest.IngredientID, opt => opt.MapFrom(src => src.ActiveIngredient_ID))
                    .ForMember(dest => dest.AllowedResidue, opt => opt.MapFrom(src => src.Residue))
                    .ForMember(dest => dest.IngredientName, opt => opt.MapFrom(src => src.ActiveIngredient.Name))
                    .ForMember(dest => dest.CommodityName, opt => opt.MapFrom(src => src.Commodity.Name))
                    .ForMember(dest => dest.ExpiredAt, opt => opt.Ignore());


                cfg.CreateMap<Data.Model.MRLProvider, Models.MrlProvider>();
            });
        }
    }
}
