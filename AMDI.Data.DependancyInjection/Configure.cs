﻿using AMDI.Data.Context;
using DryIoc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AMDI.Data.DependancyInjection
{
    public static class Configure
    {
        public static IServiceCollection AddDataLayer(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<AMDIContext>(options =>
                options.UseSqlServer(connectionString));
            return services;
        }

        public static void AddDataLayer(this Container container)
        {
            container.Register<AMDIContextFactory>(Reuse.Singleton);
            container.Register<AMDIContext>(made: Made.Of(r => ServiceInfo.Of<AMDIContextFactory>(), f => f.Create()));
        }
    }
}
